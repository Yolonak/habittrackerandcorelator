import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator, StackHeaderProps } from '@react-navigation/stack';
import { View, Text } from 'react-native';
import HabitDisplay from '../components/MainDisplayComponents/HabitDisplay';
import HabitHeaderComponent from '../components/headerComponents/HabitHeaderComponent';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const HabitsStack = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Habits"
      component={HabitDisplayScreen}
      options={{ header: (props: StackHeaderProps) => <HabitHeaderComponent {...props} /> }}
    />
  </Stack.Navigator>
);

const MetricsStack = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Metrics"
      component={MetricsScreen}
      // options={{ header: (props: StackHeaderProps) => <HabitHeaderComponent {...props} /> }}
    />
  </Stack.Navigator>
);

const HabitDisplayScreen: React.FC = () => (
  <View style={{ flex: 1 }}>
    <HabitDisplay />
  </View>
);

const MetricsScreen: React.FC = () => (
  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
    <Text>Metrics</Text>
  </View>
);

function MainDisplayScreen() {
  return (
    <Tab.Navigator screenOptions={{ headerShown: false }}>
      <Tab.Screen name="HabitsTab" component={HabitsStack} />
      <Tab.Screen name="MetricsTab" component={MetricsStack} />
    </Tab.Navigator>
  );
}

export default MainDisplayScreen;