import { getAndIncreaseIncrementAsync } from "../IncrementTableOperations";
import * as SQLite from 'expo-sqlite';

export const selectAllMetricGroups = async (db: SQLite.SQLiteDatabase) => {
    let value;
    await db.withExclusiveTransactionAsync(async (tx) => {
        value = await tx.getAllAsync(`SELECT * FROM MetricGroupTable;`);
        });
    return value;
}

export const filterMetricGroups = async (db: SQLite.SQLiteDatabase, filter: string = '') => {
    let value;
    const fullFilter = `%${filter}%`;
    await db.withExclusiveTransactionAsync(async (tx) => {
        value = await tx.getAllAsync(`SELECT * FROM MetricGroupTable
        WHERE name LIKE $filter OR description LIKE $filter;`, {$filter: fullFilter});
        });
    return value;
}

/**
 * id INTEGER PRIMARY KEY,
name TEXT UNIQUE NOT NULL,
description TEXT NOT NULL,
grpOrder INTEGER NOT NULL
* @param tx 
* @param name 
* @param description 
* @param grpOrder 
*/
export const insertMetricGroup = async (db: SQLite.SQLiteDatabase, name: string, description: string = '', grpOrder: number = 0) => {
    // validate data
    if (name.length < 1)
    {
        throw('NAME TO SHORT');
    }

    await db.withExclusiveTransactionAsync(async (tx) => {
        const idFromIncrementTable = await getAndIncreaseIncrementAsync(tx, null, 'MetricGroupTable');
        console.log(idFromIncrementTable);
        const statement = await tx.prepareAsync(`INSERT INTO MetricGroupTable
        (id, name, description, grpOrder)
        VALUES (?, ?, ?, ?);`);
        await statement.executeAsync(idFromIncrementTable, name, description, grpOrder)
        await metricGroupReorder(tx);
        });
};

export const updateMetricGroup = async (db: SQLite.SQLiteDatabase, id: number, name: string, description: string, grpOrder: number) => {
    // validate data
    if (name != null && name.length < 1)
    {
        throw(['NAME TO SHORT']);
    }

    await db.withExclusiveTransactionAsync(async (tx) => {
        await tx.runAsync(`UPDATE MetricGroupTable
        SET name = $name,
            description = $description,
            grpOrder = $grpOrder
        WHERE id = $id;`, {$name: name, $description: description, $grpOrder: grpOrder, $id: id});
        await metricGroupReorder(tx);
        });
};

export const deleteMetricGroup = async (db: SQLite.SQLiteDatabase, id: number) => {
    await db.withExclusiveTransactionAsync(async (tx) => {
        await tx.runAsync(`DELETE FROM MetricGroupTable
        WHERE id = $id;`, {$id: id});
        await metricGroupReorder(tx);
        });
};

/**
 * method for reordering metrics depending on grpOrder. It is used during inserting new metric and reordering metrics
 * @param tx transaction in which this method is called
 */
const metricGroupReorder = async (tx: any) => {
    const metricOrder = await tx.getAllAsync('SELECT id, grpOrder FROM MetricGroupTable ORDER BY grpOrder ASC');
    let order = 10;
    for (const el of metricOrder)
    {
        if (el.grpOrder !== order)
        {
            await tx.runAsync(`UPDATE MetricGroupTable
            SET grpOrder = (?)
            WHERE id = (?);`, [order, el.id]);
        }
        order += 10;
    }
};

export const insertDefaultMetricGroupAsync = async (db: SQLite.SQLiteDatabase) => {
    await db.withExclusiveTransactionAsync(async (tx) => {
      await tx.execAsync(`INSERT OR IGNORE INTO MetricGroupTable
        (id, name, description, grpOrder)
        VALUES (1, \'MetricGrp1\', \'\', 10),
        (2, \'MetricGrp2\', \'\', 20),
        (3, \'MetricGrp3\', \'\', 30);`);
    });
    console.log('INSERTED METRIC GROUP DEFAULTS')
  };