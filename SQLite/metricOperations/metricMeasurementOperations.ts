import { getAndIncreaseIncrementAsync } from "../IncrementTableOperations";
import * as SQLite from 'expo-sqlite';

export const selectAllMetricMeasurements = async (db: SQLite.SQLiteDatabase) => {
    let value;
    await db.withExclusiveTransactionAsync(async (tx) => {
        value = await tx.getAllAsync(`SELECT * FROM MetricMeasurementTable;`);
        });
    return value;
}

export const filterMetricGroups = async (db: SQLite.SQLiteDatabase, filter: string = '') => {
    let value;
    const fullFilter = `%${filter}%`;
    await db.withExclusiveTransactionAsync(async (tx) => {
        value = await tx.getAllAsync(`SELECT * FROM MetricGroupTable
        WHERE name LIKE $filter OR description LIKE $filter;`, {$filter: fullFilter});
        });
    return value;
}

/**
 * id INTEGER PRIMARY KEY,
    metricId TEXT NOT NULL,
    value TEXT NOT NULL,
    date TEXT NOT NULL,
 * @param db database
 * @param metricId id FK of metricTable
 * @param value value of the measurement
 * @param date time the measurement is taken, of format YYYY-MM-DDTHH:MM
 */
export const insertMetricMeasurement = async (
    db: SQLite.SQLiteDatabase,
    metricId: number,
    value: string,
    date: string,
    ) => {

    const errors = await metricDataMeasurementValidation(db, metricId, value, date);
    if (errors.length > 0)
    {
      throw(errors);
    }

    await db.withExclusiveTransactionAsync(async (tx) => {
        const idFromIncrementTable = await getAndIncreaseIncrementAsync(tx, null, 'MetricMeasurementTable');
        console.log(idFromIncrementTable);
        await tx.runAsync(`INSERT INTO MetricMeasurementTable
        (id, metricId, value, date)
        VALUES (?, ?, ?, ?)`,
        [idFromIncrementTable, metricId, value, date]);
      });
}

export const updateMetricMeasurement = async (db: SQLite.SQLiteDatabase, id: number, metricId: number, value: string, date: string) => {
    // validate data
    const errors = await metricDataMeasurementValidation(db, metricId, value, date);
    if (errors.length > 0)
    {
      throw(errors);
    }

    await db.withExclusiveTransactionAsync(async (tx) => {
        await tx.runAsync(`UPDATE MetricMeasurementTable
        SET value = $value,
            date = $date,
        WHERE id = $id;`, {$value: value, $date: date, $id: id});
        });
};

const metricDataMeasurementValidation = async (db: SQLite.SQLiteDatabase, metricId: number, value: string, date: string): Promise<any[]> => {
    const errors: any[] = [];
    const dateSplit = date.split('T');
    if (dateSplit.length !== 2)
    {
        errors.push('INCORRECT DATE FORMAT');
    }
    else
    {
        const time = dateSplit[1]
        const timeSplit = time.split(':')
        if (!/[0-2][0-9]:[0-6][0-9]/.test(time) || parseInt(timeSplit[0], 10) > 23 || parseInt(timeSplit[0], 10) > 59)
        {
            errors.push('INCORRECT TIME FORMAT');
        }

        const dayDate = dateSplit[0];
        if (!/[0-3][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]/.test(dayDate))
        {
            errors.push('INCORRECT DATE FORMAT');
        }
    }
    await db.withExclusiveTransactionAsync(async (tx) => {
        const metric: any = await tx.getFirstAsync('SELECT * FROM MetricTable WHERE id = $metricId', {$metricId: metricId});
        // console.log('MEASUREMENT VALIDATION:', metric, metric.metricValues.split(',').includes(value));
        if (metric == null)
        {
            errors.push(`METRIC ${metricId} DOES NOT EXIST`);
        }
        else if (!metric.metricValues.split(',').includes(value))
        {
            errors.push(`VALUE NOT IN POSSIBLE VALUES OF METRIC ${metricId}`);
        }
    });
    return errors;
}


/**
 * id INTEGER PRIMARY KEY,
    metricId TEXT NOT NULL,
    value TEXT NOT NULL,
    time TEXT NOT NULL
 * @param db 
 */
export const insertDefaultMetricMeasurementAsync = async (db: SQLite.SQLiteDatabase) => {
    await db.withExclusiveTransactionAsync(async (tx) => {
      await tx.execAsync(`INSERT OR IGNORE INTO MetricMeasurementTable
        (id, metricId, value, date)
        VALUES (1, \'1\', \'1\', '2024-01-01T12:00'),
        (2, \'1\', \'2\', '2024-01-02T12:00'),
        (3, \'1\', \'2\', '2024-01-03T12:00'),
        (4, \'1\', \'1\', '2024-01-04T13:00');`);
    });
    console.log('INSERTED METRIC MEASUREMENTS DEFAULTS')
  };