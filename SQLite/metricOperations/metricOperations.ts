import { findDuplicates } from "../../helperFunctions/Common";
import { getAndIncreaseIncrementAsync } from "../IncrementTableOperations";
import * as SQLite from 'expo-sqlite';

enum MetricType {
  NUM = 0,
  STR = 1
}

export const selectAllMetrics = async (db: SQLite.SQLiteDatabase) => {
  let value;
  await db.withExclusiveTransactionAsync(async (tx) => {
      value = await tx.getAllAsync(`SELECT * FROM MetricTable ORDER BY metricOrder ASC;`);
      });
  return value;
}

export const filterMetricsByGroup = async (db: SQLite.SQLiteDatabase, groupId: number) => {
  let value;
  await db.withExclusiveTransactionAsync(async (tx) => {
      value = await tx.getAllAsync(`SELECT * FROM MetricTable
      WHERE groupId = $groupId;`, {$groupId: groupId});
      });
  return value;
}

/**
 * 
 * @param db database connection that is opened
 * @param name name of metric
 * @param iconId iconId (foreign key to icon table)
 * @param groupId groupId (foreign key to group table)
 * @param colorCode hex code of collor used for metric
 * @param metricValues values metric can take (max 20 min 2) string separated by comma
 * @param description 
 * @param remidnersDay string separated by comma format (Mon, Tue...)
 * @param remindersTime string separated by comma format(HH:MM, HH:MM)
 * @param intervalLength number of hours between measurements
 * @param metricType 0(numeric) or 1(string)
 * @returns 
 */
export const insertMetric = async (
    db: SQLite.SQLiteDatabase,
    name: string,
    iconId: number,
    groupId: number,
    colorCode: string,
    metricValues: string,
    description: string = '',
    remindersDay: string = '',
    remindersTime: string = '',
    intervalLength: number = 24,
    metricType: MetricType = 0,
    metricOrder: number = 0,
    ) => {

    const errors = await metricDataValidation(name, iconId, groupId, colorCode, metricValues, description, remindersDay, remindersTime, intervalLength, metricType, metricOrder, db);
    // TODO add reminders validation
    if (errors.length > 0)
    {
      throw(errors);
    }

    await db.withExclusiveTransactionAsync(async (tx) => {
        const idFromIncrementTable = await getAndIncreaseIncrementAsync(tx, null, 'MetricTable');
        console.log(idFromIncrementTable);
        await tx.runAsync(`INSERT INTO MetricTable
        (id, name, iconId, groupId, colorCode, metricValues, description, remindersDay, remindersTime, intervalLength, metricType, metricOrder)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
        [idFromIncrementTable, name, iconId, groupId, colorCode, metricValues, description, remindersDay, remindersTime, intervalLength, metricType.toString(), metricOrder]);

        await metricReorder(tx, groupId);
      });
}

export const updateMetric = async (
  db: SQLite.SQLiteDatabase,
  id: number,
  name: string,
  iconId: number,
  groupId: number,
  colorCode: string,
  metricValues: string,
  description: string = '',
  remindersDay: string = '',
  remindersTime: string = '',
  intervalLength: number = 24,
  metricType: MetricType = 0,
  metricOrder: number = 0,
  ) => {
  // validate data
  const errors = await metricDataValidation(name, iconId, groupId, colorCode, metricValues, description, remindersDay, remindersTime, intervalLength, metricType, metricOrder);
  // TODO add reminders validation
  if (errors.length > 0)
  {
    throw(errors);
  }

  await db.withExclusiveTransactionAsync(async (tx) => {
      await tx.runAsync(`UPDATE MetricTable
      SET name = $name,
      iconId = $iconId,
      groupId = $groupId,
      colorCode = $colorCode,
      metricValues = $metricValues,
      description = $description,
      remindersDay = $remindersDay,
      remindersTime = $remindersTime,
      intervalLength = $intervalLength,
      metricType = $metricType,
      metricOrder = $metricOrder
      WHERE id = $id;`, {
        $name: name,
        $iconId: iconId,
        $groupId: groupId,
        $colorCode: colorCode,
        $metricValues: metricValues,
        $description: description,
        $remindersDay: remindersDay,
        $remindersTime: remindersTime,
        $intervalLength: intervalLength,
        $metricType: metricType,
        $metricOrder: metricOrder,
        $id: id
      });
      await metricReorder(tx, metricOrder);
      });
};

/**
 * method for reordering metrics depending on grpOrder. It is used during inserting new metric and reordering metrics
 * @param tx transaction in which this method is called
 */
const metricReorder = async (tx: any, groupId: number) => {
  const metricOrder = await tx.getAllAsync('SELECT id, metricOrder FROM MetricTable WHERE groupId = ? ORDER BY metricOrder ASC;', [groupId]);
  let order = 10;
  for (const el of metricOrder)
  {
      if (el.metricOrder !== order)
      {
          await tx.runAsync(`UPDATE MetricTable
          SET metricOrder = (?)
          WHERE id = (?);`, [order, el.id]);
      }
      order += 10;
  }
};

const metricDataValidation = async(
  name: string,
  iconId: number,
  groupId: number,
  colorCode: string,
  metricValues: string,
  description: string = '',
  remindersDay: string = '',
  remindersTime: string = '',
  intervalLength: number = 24,
  metricType: MetricType = 0,
  metricOrder: number = 0,
  db:SQLite.SQLiteDatabase | null = null,
  ): Promise<any[]> => {
    const errors = [];
    if (name == null || iconId == null || groupId == null || colorCode == null || metricValues == null)
    {
      errors.push('MISSING REQUIRED FIELDS');
    }
    const valuesArray = metricValues.split(',');
    if (valuesArray.length <= 1 || valuesArray.length >= 20)
    {
      errors.push('NEEDS TO HAVE LESS THEN 20 AND MORE THEN 1 METRIC VALUES');
    }
    if (valuesArray.length >= 1 && metricType === 0)
    {
      for (const el of valuesArray)
      {
        if (!/^-?\d+$/.test(el))
        {
          errors.push(`${el} IS NOT A NUMBER`);
        }
      }
    }
    else if (valuesArray.length >= 1 && metricType === 1) {
      const duplicates = findDuplicates(valuesArray);
      if (duplicates.length >= 1)
      {
        errors.push(`DUPLICATE VALUES: ${duplicates}`);
      }
    }
    if (remindersDay.length > 1){
      const splitDays = remindersDay.split(',');
      const daySet = new Set(['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']);
      for (const el of splitDays)
      {
        if (!daySet.has(el.replaceAll(' ', '')))
        {
          errors.push('WRONG DAY SYMBOL');
        }
      }
    }

    if (remindersTime.length > 1){
      const splitTime = remindersTime.split(',');
      for (const el of splitTime)
      {
        if (!/[0-2][0-9]:[0-6][0-9]/.test(el))
        {
          errors.push('WRONG TIME');
        }
      }
    }

    // TODO add validation of previous values compared to current ones
    /* if (db != null)
    {
      await db.withExclusiveTransactionAsync(async (tx) => {
        const previousValues = await tx.getFirstAsync('SELECT * FROM ')
      });
    } */
    return errors;

}
  
/**
 * id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    remindersTime TEXT NOT NULL,
    remindersDay TEXT NOT NULL,
    iconId INTEGER NOT NULL,
    colorCode TEXT NOT NULL,
    intervalLength INTEGER NOT NULL,
    metricType INTEGER NOT NULL,
    metricValues TEXT NOT NULL,
    groupId INTEGER NOT NULL,
    metricOrder INTEGER NOT NULL
 * @param db 
 */
  export const insertDefaultMetricAsync = async (db: SQLite.SQLiteDatabase) => {
    await db.withExclusiveTransactionAsync(async (tx) => {
      await tx.execAsync(`INSERT OR IGNORE INTO MetricTable
        (id, name, description, remindersDay, remindersTime, iconId, colorCode, intervalLength, metricType, metricValues, groupId, metricOrder)
        VALUES (1, \'Metric1\', \'TESTING METRIC 1\', 'Mon', '00:00' , 1, '#FF6B33', 12, 'num', '1,2,3,4,5', 1, 10),
        (2, \'Metric2\', \'TESTING METRIC 2\', '' , '' ,1, '#E6FF33', 24, 'str', 'a,b,c', 1, 20),
        (3, \'Metric3\', \'TESTING METRIC 3\', '' , '', 1, '#3386FF', 12, 'str', 'a,b', 1, 30);`);
    });
    console.log('INSERTED METRIC DEFAULTS')
  };
  