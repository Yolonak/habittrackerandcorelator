import * as SQLite from 'expo-sqlite';
/**
 *
 * @param {SQLite.SQLiteDatabase} db
 * @returns {Array}
 */
export const fetchAllIncrements = async (db: SQLite.SQLiteDatabase): Promise<any[]> => {
    let returnValue: any;
    await db.withExclusiveTransactionAsync(async (tx) => {
      returnValue = await tx.getAllAsync('SELECT * FROM IncrementsTable;');
    });
    return returnValue;
  };

  // TODO change any to Transaction once type is exported
  /**
   * increase increment and then return the value from the same query
   * @param {*} tx
   * @param {*} id id of table
   * 
   * @returns
   */
  export const getAndIncreaseIncrementAsync = async (tx: any, id: number | null = null, name: string | null = null): Promise<number> => {
    let value: any;
    // TODO returning does no return (return undefined)
    if (name == null)
    {
      const statement = await tx.prepareAsync(`UPDATE IncrementsTable
      SET value = value + 1
      WHERE id = (?)
      RETURNING value;`);
      await statement.executeAsync(id);
      await statement.finalizeAsync();

      value = await tx.getFirstAsync('SELECT value FROM IncrementsTable WHERE name = ?', [name]);
    }
    else
    {
      const statement = await tx.prepareAsync(`UPDATE IncrementsTable
      SET value = value + 1
      WHERE name = (?)
      RETURNING value;`);
      await statement.executeAsync(name);
      await statement.finalizeAsync();

      value = await tx.getFirstAsync('SELECT value FROM IncrementsTable WHERE name = ?', [name]);
    }
    if (value == null || value.value == null)
    {
      throw(`NO INCREMENT VALUE FOR TABLE ${id}, ${name}`);
    }
    return value.value;
  };
  
  // used to determine the id of other tables instead of autoincrement so we have space to add default
  // or template values without conficts with user data
  export const insertDefaultIncrementAsync = async (db: SQLite.SQLiteDatabase) => {
    console.log('inserting default increment');
    await db.withExclusiveTransactionAsync(async (tx) => {
      await tx.execAsync(`INSERT OR IGNORE INTO IncrementsTable
        (id, name, value)
        VALUES (1, \'MetricGroupTable\', 5),
        (2, 'HabitGroupTable', 10),
        (3, 'HabitTable', 10),
        (4, 'MetricTable', 10),
        (5, 'HabitMeasurementTable', 100),
        (6, 'MetricMeasurementTable', 10),
        (7, 'IconsTable', 10);`);
    });
    console.log('INSERTED DEFAULT INCREMENTS');
  };
  
  export default fetchAllIncrements;
  