// SQL ERROR IS CAUSED BY UNIQUE CONSTRAINT NOT BEING PROPERLY HANDLED BY THE LIBRARY
import * as SQLite from 'expo-sqlite';
import { insertDefaultIncrementAsync, fetchAllIncrements, getAndIncreaseIncrementAsync } from './IncrementTableOperations';
import { insertDefaultMetricAsync, insertMetric, selectAllMetrics, updateMetric } from './metricOperations/metricOperations';
import { filterMetricGroups, insertDefaultMetricGroupAsync, insertMetricGroup, selectAllMetricGroups, updateMetricGroup } from './metricOperations/metricGroupOperations';
import { insertDefaultIconAsync } from './otherOperations/iconOperations';
import { insertDefaultMetricMeasurementAsync, insertMetricMeasurement } from './metricOperations/metricMeasurementOperations';
import { insertDefaultHabitAsync, insertHabit, selectAllHabits, updateHabit } from './habitOperations/habitOperations';
import { insertDefaultHabitGroupAsync } from './habitOperations/habitGroupOperations';
import { insertDefaultHabitMeasurementAsync, insertHabitMeasurement, selectAllHabitMeasurements } from './habitOperations/habitMeasurementOperations';

export default class DatabaseInstance {
  static db: SQLite.SQLiteDatabase;

  static activeInstance: DatabaseInstance;

  static async initDatabase() {
    await this.clearDatabase();
    await DatabaseInstance.db.withExclusiveTransactionAsync(async(tx) => {
      await tx.execAsync(
        `CREATE TABLE IF NOT EXISTS IncrementsTable (
          id INTEGER PRIMARY KEY,
          name TEXT NOT NULL,
          value INTEGER NOT NULL
        );`,
      );

      await tx.execAsync(
        `CREATE TABLE IF NOT EXISTS IconsTable (
          id INTEGER PRIMARY KEY,
          nameOfAsset TEXT,
          keywords TEXT NOT NULL
        );`,
      );

      await tx.execAsync(
        `CREATE TABLE IF NOT EXISTS MetricGroupTable (
          id INTEGER PRIMARY KEY,
          name TEXT NOT NULL,
          description TEXT NOT NULL,
          grpOrder INTEGER NOT NULL
        );`,
      );

      await tx.execAsync(
        `CREATE TABLE IF NOT EXISTS HabitGroupTable (
          id INTEGER PRIMARY KEY,
          name TEXT NOT NULL,
          description TEXT NOT NULL,
          grpOrder INTEGER NOT NULL
        );`,
      );
      console.log('ALL QUERIES FOR CREATING TIER 1 TABLES FINISHED')

      // Create HabitTable
      // remonders in format 11-01-2020T00:00:00
      await tx.execAsync(
        `CREATE TABLE IF NOT EXISTS HabitTable (
          id INTEGER PRIMARY KEY,
          name TEXT NOT NULL,
          iconId INTEGER NOT NULL,
          groupId INTEGER NOT NULL,
          colorCode TEXT NOT NULL,
          remindersDay TEXT NOT NULL,
          remindersTime TEXT NOT NULL,
          targetQuantity INTEGER NOT NULL,
          habitOrder INTEGER NOT NULL,
          qualityValue BOOLEAN NOT NULL,
          minQualityValue INTEGER,
          maxQualityValue INTEGER,
          timeRelevant BOOLEAN NOT NULL,
          description TEXT NOT NULL
        );`,
      );
      /*
          ,
          FOREIGN KEY (groupId) REFERENCES HabitGroupTable(id),
          FOREIGN KEY (iconId) REFERENCES IconsTable(id)
      */

      // values are of type '0,1,2,3' for number metric or 'yes, no, maybe' for string metric
      // metricType is 'numeric' or 'string'
      await tx.execAsync(
        `CREATE TABLE IF NOT EXISTS MetricTable (
          id INTEGER PRIMARY KEY,
          name TEXT NOT NULL,
          description TEXT NOT NULL,
          remindersTime TEXT NOT NULL,
          remindersDay TEXT NOT NULL,
          iconId INTEGER NOT NULL,
          colorCode TEXT NOT NULL,
          intervalLength INTEGER NOT NULL,
          metricType INTEGER NOT NULL,
          metricValues TEXT NOT NULL,
          groupId INTEGER NOT NULL,
          metricOrder INTEGER NOT NULL
        );`,
      );

      /*
          ,
          FOREIGN KEY (iconId) REFERENCES IconsTable(id),
          FOREIGN KEY (groupId) REFERENCES MetricGroupTable(id)
      */
      console.log('ALL QUERIES FOR CREATING TIER 2 TABLES FINISHED')

      await tx.execAsync(
        `CREATE TABLE IF NOT EXISTS HabitMeasurementTable (
          id INTEGER PRIMARY KEY,
          habitId TEXT NOT NULL,
          value INTEGER,
          date TEXT NOT NULL
        );`,
        /*
          ,
            FOREIGN KEY (habitId) REFERENCES HabitTable(id)
          */
      );

      await tx.execAsync(
        `CREATE TABLE IF NOT EXISTS MetricMeasurementTable (
          id INTEGER PRIMARY KEY,
          metricId TEXT NOT NULL,
          value TEXT NOT NULL,
          date TEXT NOT NULL
        );`,
        /*
          ,
            FOREIGN KEY (metricId) REFERENCES MetricTable(id)
          */
      );
      console.log('ALL QUERIES FOR CREATING TIER 3 TABLES FINISHED');
    });
    console.log('finished creating tables');
  }

  static async testQueries()
  {
    await insertDefaultIncrementAsync(DatabaseInstance.db);
    await insertDefaultMetricGroupAsync(DatabaseInstance.db);
    await insertDefaultIconAsync(DatabaseInstance.db);
    await insertDefaultMetricAsync(DatabaseInstance.db);
    await insertDefaultMetricMeasurementAsync(DatabaseInstance.db);
    await insertDefaultHabitGroupAsync(DatabaseInstance.db);
    await insertDefaultHabitAsync(DatabaseInstance.db);
    await insertDefaultHabitMeasurementAsync(DatabaseInstance.db);

    /* await insertHabitMeasurement(DatabaseInstance.db, 1, 2, '2024-05-04T14:00:00');
    await insertHabitMeasurement(DatabaseInstance.db, 1, 3, '2024-05-04T14:00:00');
    await insertHabitMeasurement(DatabaseInstance.db, 1, 1, '2024-05-04T14:00:00');
    const habits = await selectAllHabitMeasurements(DatabaseInstance.db);
    console.log(habits); */
  }

  static async getConnection() {
    if (DatabaseInstance.db == null) {
      const dbOverride: SQLite.SQLiteDatabase = await SQLite.openDatabaseAsync('HabitsDb.db');
      // await dbOverride.closeAsync();
      // await dbOverride.deleteAsync();
      DatabaseInstance.db = dbOverride;
      // DatabaseInstance.db = dbOverride;
      // this is to enable foreign keys logic
      await DatabaseInstance.db.execAsync('PRAGMA journal_mode = WAL');
      await DatabaseInstance.db.execAsync('PRAGMA foreign_keys = ON');
    }
    return DatabaseInstance.db;
  }

  static async clearDatabase() {
    const { db } = DatabaseInstance;
    try {
      await db.withExclusiveTransactionAsync(async(tx) => {
        // Drop all tables
        await tx.execAsync('DROP TABLE IF EXISTS HabitTable;');
        await tx.execAsync('DROP TABLE IF EXISTS IconsTable;');
        await tx.execAsync('DROP TABLE IF EXISTS IncrementsTable;');
        await tx.execAsync('DROP TABLE IF EXISTS MetricGroupTable;');
        await tx.execAsync('DROP TABLE IF EXISTS HabitGroupTable;');
        await tx.execAsync('DROP TABLE IF EXISTS MetricTable;');
        await tx.execAsync('DROP TABLE IF EXISTS HabitTable;');
        await tx.execAsync('DROP TABLE IF EXISTS HabitMeasurementTable;');
        await tx.execAsync('DROP TABLE IF EXISTS MetricMeasurementTable;');
      });
      console.log('Database cleared successfully.');
    } catch (error) {
      console.error('Error clearing database:', error);
    }
  }

  static async init() {
    if (DatabaseInstance.activeInstance == null) {
      DatabaseInstance.activeInstance = await new DatabaseInstance();
    }
    await DatabaseInstance.getConnection();
    await DatabaseInstance.initDatabase();
    await DatabaseInstance.testQueries();
  }
}
