import { GroupData } from "../../commonTypes/PropTypes";
import { getAndIncreaseIncrementAsync } from "../IncrementTableOperations";
import * as SQLite from 'expo-sqlite';

export const selectAllHabitGroups = async (db: SQLite.SQLiteDatabase): Promise<GroupData[]> => {
    let value: GroupData[] = [];
    await db.withExclusiveTransactionAsync(async (tx) => {
        value = await tx.getAllAsync(`SELECT * FROM HabitGroupTable ORDER BY grpOrder ASC;`);
        });
    return value;
}

export const filterHabitGroups = async (db: SQLite.SQLiteDatabase, filter: string = '') => {
    let value;
    const fullFilter = `%${filter}%`;
    await db.withExclusiveTransactionAsync(async (tx) => {
        value = await tx.getAllAsync(`SELECT * FROM HabitGroupTable
        WHERE name LIKE $filter OR description LIKE $filter;`, {$filter: fullFilter});
        });
    return value;
}

/**
 * method for inserting habit group in sqlite
* @param db sqlite db connection.
* @param name TEXT NOT NULL
* @param description TEXT NOT NULL, default empty string
* @param grpOrder INTEGER NOT NULL
*/
export const insertHabitGroup = async (db: SQLite.SQLiteDatabase, name: string, description: string = '', grpOrder: number = 0) => {
    // validate data
    if (name.length < 1)
    {
        throw('NAME TO SHORT');
    }

    await db.withExclusiveTransactionAsync(async (tx) => {
        const idFromIncrementTable = await getAndIncreaseIncrementAsync(tx, null, 'HabitGroupTable');
        console.log(idFromIncrementTable);
        const statement = await tx.prepareAsync(`INSERT INTO HabitGroupTable
        (id, name, description, grpOrder)
        VALUES (?, ?, ?, ?);`);
        await statement.executeAsync(idFromIncrementTable, name, description, grpOrder)
        await habitGroupReorder(tx);
        });
};

/**
 * updating existing habit group
 * @param db 
 * @param id id of habit group
 * @param name name lenght more than 1
 * @param description str
 * @param grpOrder number (0, 10, 20...)
 */
export const updateHabitGroup = async (db: SQLite.SQLiteDatabase, id: number, name: string, description: string, grpOrder: number) => {
    // validate data
    if (name != null && name.length < 1)
    {
        throw(['NAME TO SHORT']);
    }

    await db.withExclusiveTransactionAsync(async (tx) => {
        await tx.runAsync(`UPDATE HabitGroupTable
        SET name = $name,
            description = $description,
            grpOrder = $grpOrder
        WHERE id = $id;`, {$name: name, $description: description, $grpOrder: grpOrder, $id: id});
        await habitGroupReorder(tx);
        });
};

export const deleteHabitGroup = async (db: SQLite.SQLiteDatabase, id: number) => {
    await db.withExclusiveTransactionAsync(async (tx) => {
        await tx.runAsync(`DELETE FROM HabitGroupTable
        WHERE id = $id;`, {$id: id});
        await habitGroupReorder(tx);
        });
};

/**
 * method for reordering habits depending on grpOrder. It is used during inserting new habit and reordering habits
 * @param tx transaction in which this method is called
 */
const habitGroupReorder = async (tx: any) => {
    const habitOrider = await tx.getAllAsync('SELECT id, grpOrder FROM HabitGroupTable ORDER BY grpOrder ASC');
    let order = 10;
    for (const el of habitOrider)
    {
        if (el.grpOrder !== order)
        {
            await tx.runAsync(`UPDATE HabitGroupTable
            SET grpOrder = (?)
            WHERE id = (?);`, [order, el.id]);
        }
        order += 10;
    }
};

/* export const insertDefaultHabitGroupAsync = async (db: SQLite.SQLiteDatabase) => {
    await db.withExclusiveTransactionAsync(async (tx) => {
      await tx.execAsync(`INSERT OR IGNORE INTO HabitGroupTable
        (id, name, description, grpOrder)
        VALUES (1, \'HabitGrp1\', \'\', 10),
        (2, \'HabitGrp2\', \'\', 20),
        (3, \'HabitGrp3\', \'\', 30);`);
    });
    console.log('INSERTED HABIT GROUP DEFAULTS');
  }; */

  export const insertDefaultHabitGroupAsync = async (db: SQLite.SQLiteDatabase) => {
    await db.withExclusiveTransactionAsync(async (tx) => {
      await tx.execAsync(`INSERT OR IGNORE INTO HabitGroupTable
      (id, name, description, grpOrder)
      VALUES 
      (1, 'Exercise Group', 'Group for exercise-related habits', 10),
      (2, 'Mindfulness Group', 'Group for mindfulness and meditation habits', 20);`);
    });
    console.log('INSERTED HABIT GROUP DEFAULTS actual');
  };