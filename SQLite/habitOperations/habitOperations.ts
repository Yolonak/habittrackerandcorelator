import { HabitData } from "../../commonTypes/PropTypes";
import { getAndIncreaseIncrementAsync } from "../IncrementTableOperations";
import * as SQLite from 'expo-sqlite';

export const selectAllHabits = async (db: SQLite.SQLiteDatabase): Promise<HabitData[]> => {
  let value: HabitData[] = [];
  await db.withExclusiveTransactionAsync(async (tx) => {
      value = await tx.getAllAsync(`SELECT * FROM HabitTable ORDER BY habitOrder ASC;`);
      });
  return value;
}

export const filterHabitsByGroup = async (db: SQLite.SQLiteDatabase, groupId: number) => {
  let value: HabitData[] = [];
  await db.withExclusiveTransactionAsync(async (tx) => {
      value = await tx.getAllAsync(`SELECT * FROM HabitTable
      WHERE groupId = $groupId;`, {$groupId: groupId});
      });
  return value;
}

/**
 * id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    iconId INTEGER NOT NULL,
    groupId INTEGER NOT NULL,
    colorCode TEXT NOT NULL,
    description TEXT NOT NULL,
    remindersDay TEXT NOT NULL,
    remindersTime TEXT NOT NULL,
    timeRelevant BOOLEAN NOT NULL,
    qualityValue BOOLEAN NOT NULL,
    minQualityValue INTEGER,
    maxQualityValue INTEGER
    habitOrder INTEGER NOT NULL
 * @param db 
 * @param name name of habit
 * @param iconId iconId of asset
 * @param groupId
 * @param colorCode
 * @param remindersDay string separated by comma format('Mon, Tue')
 * @param remindersTime string separated by comma format('HH:MM, HH:MM')
 * @param targetQuantity number , hwo much time in a day should be the target amount for this habit
 * @param habitOrder order of habit withing the group default 5 (will be first)
 * @param qualityValue boolean, signifies if habit can have a cotnained metric for quality of performance (etc grade 1-5) default false
 * @param minQualityValue number representing minimum quality of habit performance default null
 * @param maxQualityValue number representing maximum quality of habit performance default null
 * @param timeRelevant boolean, signifies if habit time is important in correlation, default false
 * @param description default ''
 */
export const insertHabit = async (
    db: SQLite.SQLiteDatabase,
    name: string,
    iconId: number,
    groupId: number,
    colorCode: string,
    remindersDay: string,
    remindersTime: string,
    targetQuantity: number = 1,
    habitOrder: number = 5,
    qualityValue: boolean = false,
    minQualityValue: number | null = null,
    maxQualityValue: number | null = null,
    timeRelevant: boolean = false,
    description: string = '',
    ) => {

    const errors = await habitDataValidation(
        name,
        iconId,
        groupId,
        colorCode,
        remindersDay,
        remindersTime,
        targetQuantity,
        habitOrder,
        qualityValue,
        minQualityValue,
        maxQualityValue,
        timeRelevant,
        description,
    );
    // TODO add reminders validation
    if (errors.length > 0)
    {
      throw(errors);
    }

    await db.withExclusiveTransactionAsync(async (tx) => {
        const idFromIncrementTable = await getAndIncreaseIncrementAsync(tx, null, 'HabitTable');
        console.log(idFromIncrementTable);
        await tx.runAsync(`INSERT INTO HabitTable
        (id, name, iconId, groupId, colorCode, remindersDay, remindersTime, targetQuantity, habitOrder, qualityValue, minQualityValue, maxQualityValue, timeRelevant, description)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
        [idFromIncrementTable, name, iconId, groupId, colorCode, remindersDay, remindersTime, targetQuantity, habitOrder, qualityValue, minQualityValue, maxQualityValue, timeRelevant, description]);

        await habitReorder(tx, groupId);
      });
}

/**
 * TODO update is ignored if id not exists, handle that case
* @param db 
 * @param name name of habit
 * @param iconId iconId of asset
 * @param groupId
 * @param colorCode
 * @param remindersDay string separated by comma format('Mon, Tue')
 * @param remindersTime string separated by comma format('HH:MM, HH:MM')
 * @param targetQuantity number , hwo much time in a day should be the target amount for this habit
 * @param habitOrder order of habit withing the group default 0 (will be first)
 * @param qualityValue boolean, signifies if habit can have a cotnained metric for quality of performance (etc grade 1-5) default false
 * @param minQualityValue number representing minimum quality of habit performance default null
 * @param maxQualityValue number representing maximum quality of habit performance default null
 * @param timeRelevant boolean, signifies if habit time is important in correlation, default false
 * @param description default ''
 */
export const updateHabit = async (
    db: SQLite.SQLiteDatabase,
    id: number,
    name: string,
    iconId: number,
    groupId: number,
    colorCode: string,
    remindersDay: string,
    remindersTime: string,
    habitOrder: number = 5,
    targetQuantity: number = 1,
    qualityValue: boolean = false,
    minQualityValue: number | null = null,
    maxQualityValue: number | null = null,
    timeRelevant: boolean = false,
    description: string = '',
  ) => {
  // validate data
  const errors = await habitDataValidation(
    name,
    iconId,
    groupId,
    colorCode,
    remindersDay,
    remindersTime,
    habitOrder,
    targetQuantity,
    qualityValue,
    minQualityValue,
    maxQualityValue,
    timeRelevant,
    description
    );
  // TODO add reminders validation
  if (errors.length > 0)
  {
    throw(errors);
  }

  await db.withExclusiveTransactionAsync(async (tx) => {
      await tx.runAsync(`UPDATE HabitTable
      SET name = $name,
      iconId = $iconId,
      groupId = $groupId,
      colorCode = $colorCode,
      remindersDay = $remindersDay,
      remindersTime = $remindersTime,
      targetQuantity = $targetQuantity,
      habitOrder = $habitOrder,
      qualityValue = $qualityValue,
      minQualityValue = $minQualityValue,
      maxQualityValue = $maxQualityValue,
      timeRelevant = $timeRelevant,
      description = $description
      WHERE id = $id;`, {
        $name: name,
        $iconId: iconId,
        $groupId: groupId,
        $colorCode: colorCode,
        $remindersDay: remindersDay,
        $remindersTime: remindersTime,
        $targetQuantity: targetQuantity,
        $habitOrder: habitOrder,
        $qualityValue: qualityValue,
        $minQualityValue: minQualityValue,
        $maxQualityValue: maxQualityValue,
        $timeRelevant: timeRelevant,
        $description: description,
        $id: id
      });
      await habitReorder(tx, groupId);
      });
};

/**
 * method for reordering habits depending on grpOrder. It is used during inserting new habit and reordering habits
 * @param tx transaction in which this method is called
 */
const habitReorder = async (tx: any, groupId: number) => {
  const habitOrder = await tx.getAllAsync('SELECT id, habitOrder FROM HabitTable WHERE groupId = ? ORDER BY habitOrder ASC;', [groupId]);
  let order = 10;
  for (const el of habitOrder)
  {
      if (el.habitOrder !== order)
      {
          await tx.runAsync(`UPDATE HabitTable
          SET habitOrder = (?)
          WHERE id = (?);`, [order, el.id]);
      }
      order += 10;
  }
};

/**
 * 
 * @param name name of habit
 * @param iconId iconId of asset
 * @param groupId
 * @param colorCode
 * @param remindersDay string separated by comma format('Mon, Tue')
 * @param remindersTime string separated by comma format('HH:MM, HH:MM')
 * @param habitOrder order of habit withing the group default 0 (will be first)
 * @param qualityValue boolean, signifies if habit can have a cotnained metric for quality of performance (etc grade 1-5) default false
 * @param minQualityValue number representing minimum quality of habit performance default null
 * @param maxQualityValue number representing maximum quality of habit performance default null
 * @param timeRelevant boolean, signifies if habit time is important in correlation, default false
 * @param description default ''
 * @param db 
 * @returns 
 */
export const habitDataValidation = async(
    name: string,
    iconId: number | null,
    groupId: number | null,
    colorCode: string,
    remindersDay: string,
    remindersTime: string,
    targetQuantity: number,
    habitOrder: number = 0,
    qualityValue: boolean = false,
    minQualityValue: number | null = null,
    maxQualityValue: number | null = null,
    timeRelevant: boolean = false,
    description: string = '',

  db:SQLite.SQLiteDatabase | null = null,
  ): Promise<any[]> => {
    const errors = [];
    if (name == null || iconId == null || groupId == null || colorCode == null)
    {
      errors.push('MISSING REQUIRED FIELDS');
    }
    if (qualityValue)
    {
        if (minQualityValue == null || maxQualityValue == null || maxQualityValue<=minQualityValue)
        {
            errors.push('WRONG VALUES, BOTH MUST BE SET AND MAX MUST ME LARGER THEN MIN');
        }
    }
    if (remindersDay.length > 1){
        const splitDays = remindersDay.split(',');
        const daySet = new Set(['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']);
        for (const el of splitDays)
        {
          if (!daySet.has(el.replaceAll(' ', '')))
          {
            errors.push('WRONG DAY SYMBOL');
          }
        }
      }
  
      if (remindersTime.length > 1){
        const splitTime = remindersTime.split(',');
        for (const el of splitTime)
        {
          if (!/[0-2][0-9]:[0-6][0-9]/.test(el))
          {
            errors.push('WRONG TIME');
          }
        }
      }

    // TODO add validation of previous values compared to current ones
    /* if (db != null)
    {
      await db.withExclusiveTransactionAsync(async (tx) => {
        const previousValues = await tx.getFirstAsync('SELECT * FROM ')
      });
    } */
    return errors;

}
  
/**
 * id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    remindersDay TEXT NOT NULL,
    remindersTime TEXT NOT NULL,
    iconId INTEGER NOT NULL,
    colorCode TEXT NOT NULL,
    timeRelevant BOOLEAN NOT NULL,
    qualityValue BOOLEAN NOT NULL,
    minQualityValue INTEGER,
    maxQualityValue INTEGER
    groupId INTEGER NOT NULL,
    habitOrder INTEGER NOT NULL
 * @param db 
 */
  /* export const insertDefaultHabitAsync = async (db: SQLite.SQLiteDatabase) => {
    await db.withExclusiveTransactionAsync(async (tx) => {
      await tx.execAsync(`INSERT OR IGNORE INTO HabitTable
      (id, name, iconId, groupId, colorCode, remindersDay, remindersTime, targetQuantity, habitOrder, qualityValue, minQualityValue, maxQualityValue, timeRelevant, description)
        VALUES (1, \'Habit1\', 1, 1, '#FF6B33', 'Mon, Wed', '12:00', 3, 10, true, 1, 5, true, 'defaultHabit1'),
        (2, \'Habit2\', 1, 1, '#337aff', 'Mon, Sat', '12:00, 18:00', 1, 20, true, 1, 5, false, 'defaultHabit2'),
        (3, \'Habit3\', 1, 2, '#000000', '', '', 10, false, null, null, 1, false, 'defaultHabit3');`);
    });
    console.log('INSERTED HABIT DEFAULTS')
  }; */
  
  export const insertDefaultHabitAsync = async (db: SQLite.SQLiteDatabase) => {
    await db.withExclusiveTransactionAsync(async (tx) => {
      await tx.execAsync(`INSERT OR IGNORE INTO HabitTable
      (id, name, iconId, groupId, colorCode, remindersDay, remindersTime, targetQuantity, habitOrder, qualityValue, minQualityValue, maxQualityValue, timeRelevant, description)
      VALUES 
      (1, 'Running', 1, 1, '#FF6B33', 'Mon, Wed', '06:00', 1, 10, true, 1, 5, true, 'Morning running session'),
      (2, 'Yoga', 1, 1, '#337AFF', 'Tue, Thu', '07:00, 18:00', 1, 20, true, 1, 5, true, 'Yoga practice'),
      (3, 'Meditation', 1, 2, '#000000', 'Mon, Wed, Fri', '08:00', 2, 30, false, null, null, false, 'Meditation session'),
      (4, 'Deep Breathing', 1, 2, '#FF33FF', 'Daily', '09:00', 5, 40, false, null, null, false, 'Deep breathing exercises');`);
    });
    console.log('INSERTED HABIT DEFAULTS actual')
  };

  export const deleteSingleHabit = async (db: SQLite.SQLiteDatabase, id: number, groupId: number | null = null) => {
    await db.withExclusiveTransactionAsync(async (tx) => {
        await tx.runAsync(`DELETE FROM HabitTable
        WHERE id = $id;`, {$id: id});
        if (groupId != null)
          {
            await habitReorder(tx, groupId);
          }
        });
};

export const deleteAllHabitsOfGroup = async (db: SQLite.SQLiteDatabase, id: number) => {
  await db.withExclusiveTransactionAsync(async (tx) => {
      await tx.runAsync(`DELETE FROM HabitTable
      WHERE groupId = $id;`, {$id: id});
      });
};