import { HabitMeasurementData } from "../../commonTypes/PropTypes";
import { getAndIncreaseIncrementAsync } from "../IncrementTableOperations";
import * as SQLite from 'expo-sqlite';

export const selectAllHabitMeasurements = async (db: SQLite.SQLiteDatabase): Promise<HabitMeasurementData[]> => {
    // TODO see how to properly handle typescript errors in this scenario without instantiating empty array
    let value: HabitMeasurementData[] = [];
    await db.withExclusiveTransactionAsync(async (tx) => {
        value = await tx.getAllAsync(`SELECT * FROM HabitMeasurementTable
        ORDER BY habitId ASC, date DESC;`);
        });
    return value;
}

export const filterHabitGroups = async (db: SQLite.SQLiteDatabase, filter: string = '') => {
    let value;
    const fullFilter = `%${filter}%`;
    await db.withExclusiveTransactionAsync(async (tx) => {
        value = await tx.getAllAsync(`SELECT * FROM HabitGroupTable
        WHERE name LIKE $filter OR description LIKE $filter;`, {$filter: fullFilter});
        });
    return value;
}

/**
 * id INTEGER PRIMARY KEY,
    habitId TEXT NOT NULL,
    value INTEGER,
    date TEXT NOT NULL,
 * @param db database
 * @param habitId id FK of habitTable
 * @param value value of the measurement
 * @param date time the measurement is taken, of format YYYY-MM-DDTHH:MM
 */
export const insertHabitMeasurement = async (
    db: SQLite.SQLiteDatabase,
    habitId: number,
    value: number | null,
    date: string,
    ) => {

    const errors = await habitDataMeasurementValidation(db, habitId, value, date);
    if (errors.length > 0)
    {
      throw(errors);
    }


    await db.withExclusiveTransactionAsync(async (tx) => {
        const idFromIncrementTable = await getAndIncreaseIncrementAsync(tx, null, 'HabitMeasurementTable');
        const miniDate = date.split('T')[0];
        // const nCompleted = await setNCompleted(tx, habitId, miniDate);
        console.log(idFromIncrementTable);
        // console.log('NCOMPLETED:', nCompleted);
        await tx.runAsync(`INSERT INTO HabitMeasurementTable
        (id, habitId, value, date)
        VALUES (?, ?, ?, ?)`,
        [idFromIncrementTable, habitId, value, date]);
      });
}

export const updateHabitMeasurement = async (db: SQLite.SQLiteDatabase, id: number, habitId: number, value: number | null, date: string) => {
    // validate data
    const errors = await habitDataMeasurementValidation(db, habitId, value, date);
    if (errors.length > 0)
    {
      throw(errors);
    }

    await db.withExclusiveTransactionAsync(async (tx) => {
        await tx.runAsync(`UPDATE HabitMeasurementTable
        SET value = $value,
        date = $date
        WHERE id = $id;`, {$value: value, $id: id, $date: date});
        });
};

/**
 * check the measurements taken on this day
 * also correct if measurements don't have properly numbered measurements
 * @param tx transaction
 * @param habitId 
 * @param date date of format YYYY-MM-DDT...
 * @returns number of measurements taken this day for the followign habit + 1
 */
const setNCompleted = async (tx: any, habitId: number, date: string): Promise<number> =>
{
    // adding Z cause date is string for comparison reason, only need measurements of that day
    const latestMeasurements = await tx.getAllAsync(
        `SELECT * FROM HabitMeasurementTable WHERE habitId = $habitId AND date >= $date AND date <= $maxDate
        ORDER BY date ASC;`,
        {$habitId: habitId, $date: date, $maxDate: date + 'Z'},
    );
    if (latestMeasurements == null || latestMeasurements.lenght === 0)
    {
        return 1;
    }
    let counter = 1;
    let correctionRequired = false;
    for (const el of latestMeasurements)
    {
        if (el.nCompleted !== counter)
        {
            console.warn('CORRECTION REQUIRED', el, counter);
            correctionRequired = true;
            break;
        }
        counter += 1;
    }
    // correct faulty data in the database
    if (correctionRequired)
    {
        counter = 1;
        for (const el of latestMeasurements)
        {
            await tx.runAsync(`UPDATE HabitMeasurementTable
            SET nCompleted = (?)
            WHERE id = (?);`, [counter, el.id]);
            counter += 1;
        }
    }
    return counter;
}

const habitDataMeasurementValidation = async (db: SQLite.SQLiteDatabase, habitId: number, value: number | null, date: string): Promise<any[]> => {
    const errors: any[] = [];
    const dateSplit = date.split('T');
    if (dateSplit.length !== 2)
    {
        errors.push('INCORRECT DATE FORMAT');
    }
    else
    {
        const time = dateSplit[1]
        const timeSplit = time.split(':')
        if (!/[0-2][0-9]:[0-6][0-9]/.test(time) || parseInt(timeSplit[0], 10) > 23 || parseInt(timeSplit[0], 10) > 59)
        {
            errors.push('INCORRECT TIME FORMAT');
        }

        const dayDate = dateSplit[0];
        if (!/[0-3][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]/.test(dayDate))
        {
            errors.push('INCORRECT DATE FORMAT');
        }
    }
    await db.withExclusiveTransactionAsync(async (tx) => {
        const habit: any = await tx.getFirstAsync('SELECT * FROM HabitTable WHERE id = $habitId', {$habitId: habitId});
        // console.log('MEASUREMENT VALIDATION:', habit, habit.habitValues.split(',').includes(value));
        if (habit == null)
        {
            errors.push(`HABIT ${habitId} DOES NOT EXIST`);
        }
        else if (value != null && habit.qualityValue == false)
        {
            errors.push(`VALUE NOT SUPPORTED ${habitId}`);
        }
        else if (value != null && (habit.minQualityValue > value || habit.maxQualityValue < value))
        {
            errors.push(`VALUE NOT IN VALID INTERVAL ${habitId}`);
        }
    });
    return errors;
}


/**
 * id INTEGER PRIMARY KEY,
    habitId TEXT NOT NULL,
    value INTEGER
    date TEXT NOT NULL,
 * @param db 
 */
/* export const insertDefaultHabitMeasurementAsync = async (db: SQLite.SQLiteDatabase) => {
    await db.withExclusiveTransactionAsync(async (tx) => {
      await tx.execAsync(`INSERT OR IGNORE INTO HabitMeasurementTable
        (id, habitId, value, date)
        VALUES (1, \'1\', null, '2024-05-01T12:00'),
        (2, \'1\', 2, '2024-05-02T12:00'),
        (3, \'1\', 2, '2024-05-03T12:00'),
        (4, \'1\', 3, '2024-05-04T12:00'),
        (5, \'1\', 1, '2024-05-04T13:00');`);
    });
    console.log('INSERTED HABIT MEASUREMENTS DEFAULTS')
  }; */

  export const insertDefaultHabitMeasurementAsync = async (db: SQLite.SQLiteDatabase) => {
    await db.withExclusiveTransactionAsync(async (tx) => {
      await tx.execAsync(`INSERT OR IGNORE INTO HabitMeasurementTable
      (id, habitId, value, date)
      VALUES 
      (1, 1, 3, '2024-05-05T06:00'),
      (2, 1, 4, '2024-05-06T06:00'),
      (3, 1, 2, '2024-05-07T06:00'),
      (4, 1, 5, '2024-05-08T06:00'),
      (5, 1, 3, '2024-05-09T06:00'),
      (6, 1, 2, '2024-05-10T06:00'),
      (7, 1, 4, '2024-05-11T06:00'),
      (8, 1, 3, '2024-05-12T06:00'),
      (9, 1, 5, '2024-05-13T06:00'),
      (10, 1, 1, '2024-05-14T06:00'),
      (11, 1, 3, '2024-06-01T06:00'),
      (12, 2, 3, '2024-05-05T07:00'),
        (13, 2, 4, '2024-05-06T18:00'),
        (14, 2, 2, '2024-05-07T07:00'),
        (15, 2, 5, '2024-05-08T18:00'),
        (16, 2, 3, '2024-05-09T07:00'),
        (17, 2, 2, '2024-05-10T18:00'),
        (18, 2, 4, '2024-05-11T07:00'),
        (19, 2, 3, '2024-05-12T18:00'),
        (20, 2, 5, '2024-05-13T07:00'),
        (21, 2, 1, '2024-05-14T18:00'),
        (22, 2, 3, '2024-06-01T07:00'),
        (23, 3, null, '2024-05-05T08:00'),
        (24, 3, null, '2024-05-06T08:00'),
        (25, 3, null, '2024-05-07T08:00'),
        (26, 3, null, '2024-05-08T08:00'),
        (27, 3, null, '2024-05-09T08:00'),
        (28, 3, null, '2024-05-10T08:00'),
        (29, 3, null, '2024-05-11T08:00'),
        (30, 3, null, '2024-05-12T08:00'),
        (31, 3, null, '2024-05-13T08:00'),
        (32, 3, null, '2024-05-14T08:00'),
        (33, 3, null, '2024-06-01T08:00'),
        (34, 4, null, '2024-05-05T09:00'),
    (35, 4, null, '2024-05-06T09:00'),
    (36, 4, null, '2024-05-07T09:00'),
    (37, 4, null, '2024-05-08T09:00'),
    (38, 4, null, '2024-05-09T09:00'),
    (39, 4, null, '2024-05-10T09:00'),
    (40, 4, null, '2024-05-11T09:00'),
    (41, 4, null, '2024-05-12T09:00'),
    (42, 4, null, '2024-05-13T09:00'),
    (43, 4, null, '2024-05-14T09:00'),
    (44, 4, null, '2024-06-01T09:00'),
    (45, 4, null, '2024-06-01T10:00'),(46, 4, null, '2024-06-01T11:00'),(47, 4, null, '2024-06-01T11:00');`);
    });
    console.log('INSERTED HABIT MEASUREMENTS DEFAULTS ACTUAL')
  };

  export const deleteSingleHabitMeasurement = async (db: SQLite.SQLiteDatabase, id: number) => {
    await db.withExclusiveTransactionAsync(async (tx) => {
        await tx.runAsync(`DELETE FROM HabitMeasurementTable
        WHERE id = $id;`, {$id: id});
        });
};

export const deleteAllHabitMeasurementsForHabit = async (db: SQLite.SQLiteDatabase, id: number) => {
    await db.withExclusiveTransactionAsync(async (tx) => {
        await tx.runAsync(`DELETE FROM HabitMeasurementTable
        WHERE habitId = $id;`, {$id: id});
        });
};