import * as SQLite from 'expo-sqlite';
import { IconData } from '../../commonTypes/PropTypes';

/**
 * id INTEGER PRIMARY KEY,
    nameOfAsset TEXT,
    keywords TEXT NOT NULL
 * @param db 
 */
export const insertDefaultIconAsync = async (db: SQLite.SQLiteDatabase) => {
    await db.withExclusiveTransactionAsync(async (tx) => {
      await tx.execAsync(`INSERT OR IGNORE INTO IconsTable
        (id, nameOfAsset, keywords)
        VALUES (1, \'favicon\', \'test, fav\'),
        (2, \'splash\', \'test, splash\');`);
    });
    console.log('INSERTED ICON DEFAULTS')
  };

export const getAllIconsAsync = async (db: SQLite.SQLiteDatabase): Promise<IconData[]> => {
  let value: IconData[] = [];
  await db.withExclusiveTransactionAsync(async (tx) => {
      value = await tx.getAllAsync(`SELECT * FROM IconsTable;`);
      });
  return value;
}