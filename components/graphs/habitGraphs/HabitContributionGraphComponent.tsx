import React from 'react';
import { View, Dimensions, StyleSheet, ScrollView } from 'react-native';
import { ContributionGraph } from 'react-native-chart-kit';

const screenWidth = Dimensions.get('window').width;

type ContributionObject = {
  date: string,
}

type ContributonGraphData = {
  data: ContributionObject[],
  colorCode: string,
}

function HabitContributionGraphComponent({ data, colorCode }: ContributonGraphData) {
  const dataComputed: any[] = [];
  const dataMap: Map<string, number> = new Map();
  // iterate trough all data and number
  // TODO consider doing this in sql
  for (const el of data)
  {
    const dateSubstr = el.date.substring(0, 10);
    const current = dataMap.get(dateSubstr) ?? 0;
    dataMap.set(dateSubstr, current + 1);
  }
  for (const [date, count] of dataMap)
  {
    dataComputed.push({date, count});
  }
  return (
    <ScrollView horizontal={true} contentContainerStyle={styles.container}>
      <ContributionGraph
        values={dataComputed}
        endDate={new Date()}
        numDays={200}
        width={screenWidth - 20}
        height={220}
        chartConfig={{
          backgroundColor: "#feefe1",
          backgroundGradientFrom: "#fb9d9d",
          backgroundGradientTo: "#fbc1c196",
          decimalPlaces: 2, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
          labelColor: (opacity = 1) => `rgba(0, 0, 255, ${opacity})`,
          style: {
            borderRadius: 16,
          },
          propsForDots: {
            r: '6',
            strokeWidth: '2',
            stroke: '#ff0000',
          },
        }}
        style={{
          marginVertical: 8,
          borderRadius: 16,
        }}
        tooltipDataAttrs={(value) => {return {}}}
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5fcff',
  },
});

export default HabitContributionGraphComponent;