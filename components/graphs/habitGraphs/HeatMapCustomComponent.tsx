import React from 'react';
import { View, ScrollView,StyleSheet, Dimensions } from 'react-native';
import Svg, { Rect, Text as SvgText } from 'react-native-svg';

const daysOfWeek = ['Mon', 'Tue', 'Wen', 'Thu', 'Fri', 'Sat', 'Sun'];
const months = ['Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'];

const { width: windowWidth } = Dimensions.get('window');
const heatmapWidth = windowWidth - 20; // Subtract padding/margins if necessary
const heatmapHeight = 300; // Fixed height for the heatmap

// TODO this component is not yet finished and requires a lot of configurations
// i have given uo on it as it appears to suffer with initial loading the same as other two components despite it being lower level

const generateHeatmapData = () => {
  // Generate random data for demonstration purposes
  const data = [];
  for (let i = 0; i < 52; i++) { // 52 weeks
    const week = [];
    for (let j = 0; j < 7; j++) { // 3 days a week (M, W, F)
      week.push(Math.floor(Math.random() * 5)); // Random intensity between 0 and 4
    }
    data.push(week);
  }
  return data;
};

const HeatMapCustomComponent = () => {
  const data = generateHeatmapData();
  console.log(data);

  return (
    <View style={{ padding: 10, height: 300, width: '100%' }}>
      <Svg height={heatmapHeight} width={heatmapWidth}>
        {/* Render Months */}
        {months.map((month, i) => (
          <SvgText key={i} x={(i + 1) * 60} y="15" fontSize="10" textAnchor="middle">{month}</SvgText>
        ))}
        {/* Render Days of the Week */}
        {daysOfWeek.map((day, i) => (
          <SvgText key={i} x="10" y={(i + 2) * 30} fontSize="10" textAnchor="middle">{day}</SvgText>
        ))}
        {/* Render Heatmap Cells */}
        {data.map((week, i) => (
          week.map((value, j) => (
            <Rect
              key={`${i}-${j}`}
              x={(i + 1) * 60}
              y={(j + 1) * 30}
              width="20"
              height="20"
              fill={`rgba(0, 0, 255, ${value / 4})`} // Adjust color intensity based on value
            />
          ))
        ))}
      </Svg>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5fcff',
  },
});

export default HeatMapCustomComponent;