import React from 'react';
import { View, Dimensions, StyleSheet, ScrollView, Text } from 'react-native';
import CalendarHeatmap from 'react-native-calendar-heatmap';
import { findMaxByField } from '../../../helperFunctions/Common';

// const screenWidth = Dimensions.get('window').width;

type ContributionObject = {
  date: string,
  count?: number,
}

type ContributonGraphData = {
  data: ContributionObject[],
  colorCode: string,
}

function HabitHeatMapComponent({ data, colorCode }: ContributonGraphData) {
  // const maxDate = findMaxByField(data, 'date');
  const maxDate = new Date();
  /* return (
    <View style={styles.container}>
      <CalendarHeatmap
        endDate={new Date(maxDate)}
        numDays={100}
        values={data}
        />
    </View>
  ); */
  if (data.length === 0)
  {
    return(
      <View>
        <Text>{'MISSING DATA'}</Text>
      </View>
    )
  }
  return (
    <ScrollView  horizontal={true} contentContainerStyle={styles.container}>
      <CalendarHeatmap
        endDate={maxDate}
        numDays={200}
        values={data}
        />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5fcff',
  },
});

export default HabitHeatMapComponent;