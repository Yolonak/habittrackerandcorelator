import React from 'react';
import { View, Dimensions, StyleSheet, ScrollView, Text } from 'react-native';
import CalendarHeatmap from 'react-native-calendar-heatmap-count';
import { findMaxByField } from '../../../helperFunctions/Common';

// const screenWidth = Dimensions.get('window').width;

type ContributionObject = {
  date: string,
}

type ContributionComputedObject = {
    date: string,
    count: number,
  }

type ContributonGraphData = {
  data: ContributionObject[],
  colorCode: string,
}

function HabitHeatMapCountComponent({ data, colorCode }: ContributonGraphData) {
  // const maxDate = findMaxByField(data, 'date');
  const maxDate = new Date();
  const dataComputed: ContributionComputedObject[] = [];
  const dataMap: Map<string, number> = new Map();
  // iterate trough all data and number
  // TODO consider doing this in sql
  for (const el of data)
  {
    const dateSubstr = el.date.substring(0, 10);
    const current = dataMap.get(dateSubstr) ?? 0;
    dataMap.set(dateSubstr, current + 1);
  }
  for (const [date, count] of dataMap)
  {
    dataComputed.push({date, count});
  }
  console.log(dataComputed);
  /* return (
    <View style={styles.container}>
      <CalendarHeatmap
        endDate={new Date(maxDate)}
        numDays={100}
        values={data}
        />
    </View>
  ); */
  if (data.length === 0)
  {
    return(
      <View>
        <Text>{'MISSING DATA'}</Text>
      </View>
    )
  }
  return (
    <ScrollView  horizontal={true} contentContainerStyle={styles.container}>
      <CalendarHeatmap
        endDate={maxDate}
        numDays={100}
        values={dataComputed}
        />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5fcff',
  },
});

export default HabitHeatMapCountComponent;