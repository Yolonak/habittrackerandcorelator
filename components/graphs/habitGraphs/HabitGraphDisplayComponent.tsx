import React, { useRef, useState, useEffect } from 'react';
import {
  View, Text, StyleSheet, SafeAreaView,
  ActivityIndicator
} from 'react-native';
import HabitHeatMapComponent from '../../graphs/habitGraphs/HabitHeatMapComponents';
// import HeatMapCustomComponent from '../../graphs/habitGraphs/HeatMapCustomComponent';
// import HabitHeatMapCountComponent from '../../graphs/habitGraphs/HabitHeatMapCountComponent';
import { HabitMeasurementData } from '../../../commonTypes/PropTypes';
import { Button } from 'react-native-paper';
import BarFrequencyGraph from './BarFrequencyGraph';
import HabitListComponent from './HabitListComponent';


type HabitGraphDisplayProps = {
    filteredHabitMeasurements: HabitMeasurementData[],
    colorCode: string
}

function HabitGraphDisplayComponent({ filteredHabitMeasurements, colorCode }: HabitGraphDisplayProps) {

    if (globalThis.debugMode)
    {
      console.log('RENDER HabitGraphDisplayComponent')
    }

    const graphTypes = ['List', 'Bar', 'Heat']
    const [selectedGraph, setSelectedGraph] = useState(0);
    const [loading, setLoading] = useState(true);

    const changeDisplayType = () => {
        setSelectedGraph((prevState) => {
            if (prevState + 1 >= graphTypes.length)
            {
                return 0;
            }
            return prevState + 1;
        })
    }

    useEffect(() => {
      // Simulate data fetching
      setTimeout(() => {
        setLoading(false);
      }, 10); // Adjust the timeout as needed
    }, []);

    if (loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }

  return (
    <View style={styles.container}>
        <View style={styles.leftContainer}>
            <Button onPress={changeDisplayType}>CDT</Button>
        </View>
        <SafeAreaView style={styles.rightContainer}>
            {graphTypes[selectedGraph] === 'List' && (
                /* <HabitContributionGraphComponent data={habitMeasurementList.filter((el)=> el.habitId === filterValue)} colorCode={''}/> */
                <View style={{flex: 1, height: 300}}>
                  <HabitListComponent data={filteredHabitMeasurements}/>
                </View>
            /* <HabitHeatMapCountComponent data={habitMeasurementList.filter((el)=> el.habitId === filterValue)} colorCode={''}/> */
            /*<HeatMapCustomComponent /> */
            )}
            {graphTypes[selectedGraph] === 'Heat' && (
                /* <HabitContributionGraphComponent data={habitMeasurementList.filter((el)=> el.habitId === filterValue)} colorCode={''}/> */
            <HabitHeatMapComponent data={filteredHabitMeasurements} colorCode={''}/>
            /* <HabitHeatMapCountComponent data={habitMeasurementList.filter((el)=> el.habitId === filterValue)} colorCode={''}/> */
            /*<HeatMapCustomComponent /> */
            )}
            {graphTypes[selectedGraph] === 'Bar' && (
                /* <HabitContributionGraphComponent data={habitMeasurementList.filter((el)=> el.habitId === filterValue)} colorCode={''}/> */
            <BarFrequencyGraph data={filteredHabitMeasurements}/>
            /* <HabitHeatMapCountComponent data={habitMeasurementList.filter((el)=> el.habitId === filterValue)} colorCode={''}/> */
            /*<HeatMapCustomComponent /> */
            )}
        </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderColor: '#020202',
    padding: 5,
    height: '100%',
    width: '85%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 10,
    flex: 1,
  },
  leftContainer: {
    flex: 1,
    alignItems: 'center',
  },
  rightContainer: {
    flex: 8,
    alignItems: 'center',
  },
  name: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 5,
  },
});
export default HabitGraphDisplayComponent;
