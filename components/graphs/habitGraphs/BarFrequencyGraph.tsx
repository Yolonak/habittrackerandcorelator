import React, { useState } from 'react';
import { View, StyleSheet, Dimensions, TouchableWithoutFeedback, ScrollView } from 'react-native';
import { BarChart } from 'react-native-gifted-charts';
// import { BarChart } from 'react-native-chart-kit';

const screenWidth = Dimensions.get('window').width;

interface DataPoint {
  value: number;
  date: string;
}

interface BarGraphProps {
  data: DataPoint[];
}

function BarFrequencyGraph({ data }: BarGraphProps) {
    const [parentWidth, setParentWidth] = useState(0);
    const handleLayout = (event: any) => {
        const { width } = event.nativeEvent.layout;
        setParentWidth(width);
        console.log(width);
      };
  // Function to process the data into a format suitable for the BarChart component
  const processData = (data: DataPoint[]) => {
    const dateMap: { [date: string]: { count: number, total: number } } = {};

    data.forEach((point) => {
      if (!dateMap[point.date]) {
        dateMap[point.date] = { count: 0, total: 0 };
      }
      dateMap[point.date].count += 1;
      dateMap[point.date].total += point.value;
    });

    return Object.keys(dateMap).map((date) => ({
      value: dateMap[date].count,
      avgValue: dateMap[date].total / dateMap[date].count,
      label: date,
    }));
  };

  const barData = processData(data).map((item) => ({
    value: item.value,
    label: item.label.substring(5, 10),
    frontColor: '#72B4A3', // Frequency bar color
  }));

  const avgData = processData(data).map((item) => ({
    value: item.avgValue,
    label: item.label,
    frontColor: '#E4572E', // Average value bar color
  }));

  const labelWidth = 80; // Width of each label
  const chartWidth = 1 * labelWidth;

  return (
      <ScrollView style={styles.container} horizontal={true}>
        <TouchableWithoutFeedback onPress={()=>{}}>
          <View>
          <BarChart
            data={barData}
            barWidth={30}
            spacing={20}
            disableScroll
          />
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "90%",
    borderWidth: 1,
    borderRadius : 20,
    paddingHorizontal: "2%"
  },
});

export default BarFrequencyGraph;