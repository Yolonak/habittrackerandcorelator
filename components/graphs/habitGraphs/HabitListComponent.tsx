import React, { useState } from 'react';
import { View, Text, FlatList, StyleSheet, TouchableWithoutFeedback, ScrollView, Alert } from 'react-native';
import GenericEditButton from '../../UI/Buttons/GenericEditButton'; // Ensure this component is imported correctly
import { HabitMeasurementData } from '../../../commonTypes/PropTypes';
import ChangeHabitMeasurementComponent from '../../formComponents/ChangeHabitMeasurementComponent';
import GenericDeleteButton from '../../UI/Buttons/GenericDeleteButton';
import { deleteSingleHabitMeasurement } from '../../../SQLite/habitOperations/habitMeasurementOperations';
import DatabaseInstance from '../../../SQLite/sqlInit';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../../store/store';
import { getAllHabitMeasurements } from '../../../store/habitActions';

type HabitListComponentProps = {
    data: HabitMeasurementData[]
}

type RenderItemProps = {
    item: HabitMeasurementData
}

const HabitListComponent = ({ data }: HabitListComponentProps) => {

  if (globalThis.debugMode)
  {
    console.log('RENDER HabitListComponent')
  }

  const dispatch = useDispatch<AppDispatch>();

  const [items, setItems] = useState<HabitMeasurementData[]>(data.slice(0, 10));
  const [offset, setOffset] = useState(10);

  const deleteFunction = async(id: number) => {
    try {
      const dbConn = await DatabaseInstance.getConnection();
      await deleteSingleHabitMeasurement(dbConn, id);
      dispatch(getAllHabitMeasurements());
      setItems(items.filter((el) => el.id !== id));
      console.log('DELETED DATA');
  }
  catch (error)
  {
      console.warn(error);
      Alert.alert('Error', 'Failed to delete data');
  }
  }

  const renderItem = ({ item }: RenderItemProps) => {
    const childProps = {
        props: {
          id: item.id,
          habitId: item.habitId,
          date: item.date,
          value: item.value,
          type: 'edit',
        },
        component: ChangeHabitMeasurementComponent
      }
    return (
    <TouchableWithoutFeedback onPress={()=>{}}>
      <View style={styles.row}>
        <Text style={styles.date}>{item.date}</Text>
        <Text style={styles.value}>{item.value}</Text>
        <GenericEditButton {...childProps}/>
        <GenericDeleteButton onDelete={deleteFunction} params={{id: item.id}}/>
      </View>
    </TouchableWithoutFeedback>)
  };

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}: any) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 20;
 }

  return (
    <ScrollView style={styles.container}
      onMomentumScrollEnd={({nativeEvent})=>{
        if(isCloseToBottom(nativeEvent)){
           if (data.length > offset)
            {
              setItems(items.concat(data.slice(offset, offset + 30)))
              setOffset(offset + 30)
            }
        }
        }}
        nestedScrollEnabled = {true}>
        <FlatList style={{height:"100%"}}
        data={items}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
        />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    width: "100%",
  },
  date: {
    fontSize: 16,
  },
  value: {
    fontSize: 16,
    textAlign: 'center',
  },
});

export default HabitListComponent;