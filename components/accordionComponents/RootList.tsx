import React, { useState, useEffect } from 'react';
import {
  FlatList, View, TouchableHighlight, StyleSheet, Text
} from 'react-native';
import DatabaseInstance from '../../SQLite/sqlInit';
import { GroupData, HabitData, HabitRootElemetProps, RootElementProps } from '../../commonTypes/PropTypes';

// react dynamic component and props
// https://stackoverflow.com/questions/64968743/how-to-pass-props-to-a-component-passed-as-props-in-react

type RootListProps = {
    rootComponentElement: React.FC<RootElementProps>,
    grpData: GroupData[],
}

type NumericKey = {
  [key: number]: boolean
}

function RootList({
  rootComponentElement, grpData
}: RootListProps) {

  const Component = rootComponentElement;
  const initialObject: NumericKey = {};
  grpData.map((k) => initialObject[k.id] = false);
  const [expandedStates, setExpandedState] = useState(initialObject);


  const extendHandler = (item: any) => {
    setExpandedState((prevState: any) => {
      const newState = { ...prevState, [item]: !prevState[item] };
      return newState;
    });
  };

  // <View style={styles.listContainer}><FridgeList data={data} /></View>

  return (
    <View style={styles.container}>
      <FlatList
        data={Object.values(grpData)}
        renderItem={({ item, separators }) => {
          if (item == null)
          {
            return null;
          }
          return (
          <View style={styles.container}>
            <TouchableHighlight
              key={item.id}
              onPress={() => {
                extendHandler(item.id);
              }}
              onShowUnderlay={separators.highlight}
              onHideUnderlay={separators.unhighlight}
            >
              <View style={styles.container}>
              <Component groupData={item} expanded={expandedStates[item.id] ?? false}/>
              </View>
            </TouchableHighlight>
          </View>
        )}}
        keyExtractor={(item) => {
          if (item != null){
            return item.id.toString();
          }
          else {
            return '';
          }
        }}
      />
    </View>
    /* <View style={styles.container}>
      <Component groupData={grpData[0]} expanded={true}/>
    </View> */
    /*
    { expandedStates[item.id] && (
              <View style={styles.container}>
                <View><Text>{'LALALALA'}</Text></View>
              </View>
              ) }
    */
  );
}

export default RootList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
