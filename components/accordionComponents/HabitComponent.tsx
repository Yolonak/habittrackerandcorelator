import React from 'react';
import {
  View, Text, StyleSheet, TouchableWithoutFeedback,
  Alert
} from 'react-native';
import { HabitRootElemetProps } from '../../commonTypes/PropTypes';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { AppDispatch, RootState } from '../../store/store';
import GenericEditButton from '../UI/Buttons/GenericEditButton';
import ChangeHabitComponent from '../formComponents/ChangeHabitComponent';
import HabitGraphDisplayComponent from '../graphs/habitGraphs/HabitGraphDisplayComponent';
import GenericDeleteButton from '../UI/Buttons/GenericDeleteButton';
import { deleteAllHabitMeasurementsForHabit } from '../../SQLite/habitOperations/habitMeasurementOperations';
import DatabaseInstance from '../../SQLite/sqlInit';
import { deleteSingleHabit } from '../../SQLite/habitOperations/habitOperations';
import { getAllHabitMeasurements, getAllHabits } from '../../store/habitActions';
import ChangeHabitMeasurementComponent from '../formComponents/ChangeHabitMeasurementComponent';
import GenericInsertButton from '../UI/Buttons/GenericInsertButton';

// react dynamic component and props
// https://stackoverflow.com/questions/64968743/how-to-pass-props-to-a-component-passed-as-props-in-react

function HabitComponent({ habitData, expanded }: HabitRootElemetProps) {

  if (globalThis.debugMode)
  {
    console.log('RENDER HabitComponent')
  }
  // TODO consider getting it with query here
  // const { habitMeasurementList } = useSelector((state: RootState) => state.habits);
  const filterValue = habitData.id.toString();
  const dispatch = useDispatch<AppDispatch>();
  const habitMeasurementData = useSelector((state: RootState) => state.habits.habitMeasurementList.filter((el)=> el.habitId === filterValue), shallowEqual);
  // const group = habitGroupList.find(group => group.id === habitData.groupId);

  const deleteFunction = async(id: number) => {
    try {
      const dbConn = await DatabaseInstance.getConnection();
      await deleteAllHabitMeasurementsForHabit(dbConn, id);
      await deleteSingleHabit(dbConn, id);
      dispatch(getAllHabitMeasurements());
      dispatch(getAllHabits());
      console.log('DELETED DATA');
    }
    catch (error)
    {
        console.warn(error);
        Alert.alert('Error', 'Failed to delete data');
    }
    }

  const childProps = {
    props: {
      id: habitData.id,
      name: habitData.name,
      description: habitData.description,
      habitOrder: habitData.habitOrder,
      iconId: habitData.iconId,
      groupId: habitData.groupId,
      // groupName: group?.name,
      colorCode: habitData.colorCode,
      remindersDay: habitData.remindersDay,
      remindersTime: habitData.remindersTime,
      targetQuantity: habitData.targetQuantity,
      qualityValue: habitData.qualityValue,
      minQualityValue: habitData.minQualityValue,
      maxQualityValue: habitData.maxQualityValue,
      timeRelevant: habitData.timeRelevant,
      type: 'edit',
    },
    component: ChangeHabitComponent
  }

  const childPropsInsert = {
    props: {
      id: null,
      habitId: habitData.id,
      date: null,
      value: null,
      type: 'insert',
    },
    component: ChangeHabitMeasurementComponent
  }
  return (
    <View>
      <View style={styles.container}>
        <View style={styles.leftContainer}>
          <Text>{habitData.name}</Text>
        </View>
        <View style={styles.rightContainer}>
            <GenericInsertButton {...childPropsInsert} />
            <GenericEditButton {...childProps}/>
            <GenericDeleteButton onDelete={deleteFunction} params={{id: habitData.id}}/>
        </View>
      </View>
      <TouchableWithoutFeedback>
      <View style={styles.container}>
      { /* <HabitGraphDisplayComponent filteredHabitMeasurements={habitMeasurementList.filter((el)=> el.habitId === filterValue)} colorCode={''}/> */ }
        {expanded && (
          <HabitGraphDisplayComponent filteredHabitMeasurements={habitMeasurementData} colorCode={''}/>
        )}
      </View>
      </TouchableWithoutFeedback>
    </View>
  );
  // TOUCHABLE WITHOUT FEEDBACK IS NECESARY BECAUSE OTHERWISE CLICKING ON THE LIST WILL BE EQV TO CLICKING ONA HABIT COMPONENT (WILL COLLAPSE THE GRAPH DISPLAY COMPONENT)
}

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderColor: '#020202',
    padding: 5,
    height: '100%',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 10,
    flex: 1,
  },
  leftContainer: {
    flex: 2,
    alignItems: 'center',
  },
  rightContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
  },
  name: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 5,
  },
});
export default HabitComponent;
