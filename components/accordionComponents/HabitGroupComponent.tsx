import React, { useRef } from 'react';
import {
  View, Text, StyleSheet,
  Alert,
} from 'react-native';
import InsertHabitGroupComponent from '../formComponents/ChangeHabitGroupComponent';
import { RootElementProps } from '../../commonTypes/PropTypes';
import HabitList from './HabitList';
import HabitComponent from './HabitComponent';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { AppDispatch, RootState } from '../../store/store';
import GenericEditButton from '../UI/Buttons/GenericEditButton';
import GenericDeleteButton from '../UI/Buttons/GenericDeleteButton';
import DatabaseInstance from '../../SQLite/sqlInit';
import { deleteSingleHabit, filterHabitsByGroup } from '../../SQLite/habitOperations/habitOperations';
import { deleteAllHabitMeasurementsForHabit } from '../../SQLite/habitOperations/habitMeasurementOperations';
import { getAllHabitGroups, getAllHabitMeasurements, getAllHabits } from '../../store/habitActions';
import { deleteHabitGroup } from '../../SQLite/habitOperations/habitGroupOperations';
import GenericInsertButton from '../UI/Buttons/GenericInsertButton';
import ChangeHabitComponent from '../formComponents/ChangeHabitComponent';

// react dynamic component and props
// https://stackoverflow.com/questions/64968743/how-to-pass-props-to-a-component-passed-as-props-in-react

function HabitGroupComponent({ groupData, expanded }: RootElementProps) {
  const dispatch = useDispatch<AppDispatch>();
  const deleteFunction = async(id: number) => {
    try {
      const dbConn = await DatabaseInstance.getConnection();
      const habitsToDelete = await filterHabitsByGroup(dbConn, id);
      for (const habit of habitsToDelete)
      {
        await deleteAllHabitMeasurementsForHabit(dbConn, habit.id);
        await deleteSingleHabit(dbConn, habit.id);
      }
      await deleteHabitGroup(dbConn, id);
      dispatch(getAllHabitMeasurements());
      dispatch(getAllHabits());
      dispatch(getAllHabitGroups());
      console.log('DELETED DATA');
    }
    catch (error)
    {
        console.warn(error);
        Alert.alert('Error', 'Failed to delete data');
    }
    }
  
  const childProps = {
    props: {
      id: groupData.id,
      name: groupData.name,
      description: groupData.description,
      grpOrder: groupData.grpOrder,
      type: 'edit',
    },
    component: InsertHabitGroupComponent
  }
  const childPropsInsert = {
    props: {
      id: null,
      name: null,
      description: null,
      habitOrder: null,
      iconId: null,
      groupId: null,
      // groupName: group?.name,
      colorCode: null,
      remindersDay: null,
      remindersTime: null,
      targetQuantity: null,
      qualityValue: null,
      minQualityValue: null,
      maxQualityValue: null,
      timeRelevant: null,
      type: 'insert',
    },
    component: ChangeHabitComponent
  }
  // const { habitList } = useSelector((state: RootState) => state.habits);
  const habitData = useSelector((state: RootState) => state.habits.habitList.filter((el) => el.groupId === groupData.id), shallowEqual);
  return (
    <View style={{flex: 1}}>
      <View style={styles.container}>
        <View style={styles.leftContainer}>
          <Text>{(expanded) ? 'E' : '>'}</Text>
        </View>
        <View style={styles.middleContainer}>
          <Text style={styles.name}>{groupData.name}</Text>
        </View>
        <View style={styles.rightContainer}>
          {/*<EditButtonHabitGroup
            id={groupData.id}
            name={groupData.name}
            description={groupData.description}
            grpOrder={groupData.grpOrder}
  type={'edit'} />*/}
          <GenericInsertButton {...childPropsInsert}/>
          <GenericEditButton {...childProps}
            />
          <GenericDeleteButton onDelete={deleteFunction} params={{id: groupData.id}}/>
        </View>
      </View>
      <View style={styles.expandedContainer}>
        {expanded && (
              <View style={styles.container}>
                { /* <HabitList rootComponentElement={HabitComponent} habitData={habitList.filter((el) => el.groupId === groupData.id)} /> */ }
                <HabitList rootComponentElement={HabitComponent} habitData={habitData} />
              </View>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderColor: '#020202',
    padding: 5,
    height: '100%',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 10,
    flex: 1,
  },
  leftContainer: {
    flex: 1,
    alignItems: 'center',
  },
  middleContainer: {
    flex: 5,
    alignItems: 'center',
  },
  rightContainer: {
    flex: 2,
    alignItems: 'center',
    flexDirection: 'row',
    paddingRight: 35
  },
  expandedContainer: {
    flex: 1
  },
  name: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 5,
  },
});
export default HabitGroupComponent;
