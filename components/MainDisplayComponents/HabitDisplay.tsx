import React, { useEffect } from 'react';
import {
  View, StyleSheet,
} from 'react-native';
import RootList from '../accordionComponents/RootList';
import HabitGroupComponent from '../accordionComponents/HabitGroupComponent';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../store/store';
import { getAllHabitGroups, getAllHabitMeasurements, getAllHabits } from '../../store/habitActions';
import { getAllIcons } from '../../store/iconActions';

// react dynamic component and props
// https://stackoverflow.com/questions/64968743/how-to-pass-props-to-a-component-passed-as-props-in-react

function HabitDisplay() {

  if (globalThis.debugMode)
  {
    console.log('RENDER HabitDisplay')
  }

  const { habitGroupList /*, habitList, habitMeasurementList */ } = useSelector((state: RootState) => state.habits);
  const dispatch = useDispatch<AppDispatch>();

  useEffect(() => {
    dispatch(getAllHabitGroups());
    dispatch(getAllHabits());
    dispatch(getAllHabitMeasurements());
    dispatch(getAllIcons());
    /* console.log('HGL', habitGroupList);
    console.log('HL', habitList);
    console.log('HML', habitMeasurementList); */
  }, []);

  // old code without redux
  /* const [habitGroupData, setHabitGroupData] = useState<any>([])
  useEffect(() => {
    async function loadData() {
      const dbConn = await DatabaseInstance.getConnection();
      const habitGrpData = await selectAllHabitGroups(dbConn);
      setHabitGroupData(habitGrpData);
    }
    loadData();
  }, []) */

  return (
    <View style={styles.container}>
      <RootList rootComponentElement={HabitGroupComponent} grpData={habitGroupList}/>
    </View>
  );
}

export default HabitDisplay;

const styles = StyleSheet.create({
  container: {
    // height: '100%',
    // width: '100%'
    flex: 1
  },
});
