import React, { useState, forwardRef, useImperativeHandle } from 'react';
import { View, TextInput, Button, StyleSheet, Alert, ScrollView, Text } from 'react-native';

// Assuming this method exists in another file and is imported here
import { insertHabitGroup, selectAllHabitGroups, updateHabitGroup } from '../../SQLite/habitOperations/habitGroupOperations';
import DatabaseInstance from '../../SQLite/sqlInit';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../store/store';
import { getAllHabitGroups } from '../../store/habitActions';

type HabitGroupProps = {
  id: number | null,
  name: string | null,
  description: string | null,
  grpOrder: number | null,
  ref: any,
  type: string,
}

const ChangeHabitGroupComponent: React.FC<HabitGroupProps> = forwardRef((props: HabitGroupProps, ref) => {
  const [name, setName] = useState(props.name ?? '');
  const [description, setDescription] = useState(props.description ?? '');
  const [groupOrder, setGroupOrder] = useState(props.grpOrder?.toString() ?? '5');
  const [prettyOrder, setPrettyOrder] = useState((Number.parseInt(props.grpOrder?.toString() ?? '10') / 10).toString());

  // dispatch necesary to reload the data in store
  // TODO switch to new action for adding data from form rather then reading from db if adding external database, for sqlite this is fine
  const dispatch = useDispatch<AppDispatch>();

  useImperativeHandle(ref, () => ({
    externalSubmit: async () => {
      console.log('SUBMITTING');
      await handleSubmit();
      dispatch(getAllHabitGroups());
      return;
    },
 }));

 const setOrder = (args: string) => {
  setPrettyOrder(args);
  setGroupOrder(`${args}0`);
 }

  const handleSubmit = async () => {
    if (!name) {
      Alert.alert('Error', 'Name required');
      return;
    }

    try {
        const dbConn = await DatabaseInstance.getConnection();
        if (props.type === 'insert')
        {
          await insertHabitGroup(dbConn, name, description, parseInt(groupOrder));
        }
        else
        {
          if (props.id == null)
          {
            console.warn('MISSING GROUP ID', props.id);
            return;
          }
          await updateHabitGroup(dbConn, props.id, name, description, parseInt(groupOrder))
        }
        Alert.alert('Success', 'Data saved successfully');
        setName('');
        setDescription('');
        setGroupOrder('');
    } catch (error) {
      console.warn(error);
      Alert.alert('Error', 'Failed to save data');
    }
  };

  const handleCancel = async () => {
    setName('');
    setDescription('');
    setGroupOrder('');
  }

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text>{'Name'}</Text>
      <TextInput
        style={styles.input}
        placeholder="Name"
        value={name}
        onChangeText={setName}
      />
      <Text>{'Description'}</Text>
      <TextInput
        style={styles.input}
        placeholder="Description"
        value={description}
        onChangeText={setDescription}
      />
      <Text>{'Display Order'}</Text>
      <TextInput
        style={styles.input}
        placeholder="Group Order"
        value={prettyOrder}
        onChangeText={setOrder}
        keyboardType="numeric"
      />
    </ScrollView>
  );
});

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    padding: 16,
  },
  actionButtons: {
    flexDirection: 'row',
  },
  input: {
    height: 40,
    borderColor: '#ccc',
    borderWidth: 1,
    marginBottom: 12,
    padding: 8,
  },
});

export default ChangeHabitGroupComponent;