import React, { useState, forwardRef, useImperativeHandle } from 'react';
import { View, TextInput, Button, StyleSheet, Alert, ScrollView, Text } from 'react-native';

// Assuming this method exists in another file and is imported here
import DatabaseInstance from '../../SQLite/sqlInit';
import { habitDataValidation, insertHabit, selectAllHabits, updateHabit } from '../../SQLite/habitOperations/habitOperations';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../store/store';
import { getAllHabitMeasurements } from '../../store/habitActions';
import { insertHabitMeasurement, updateHabitMeasurement } from '../../SQLite/habitOperations/habitMeasurementOperations';
import DatePicker from './Pickers/DatePicker';
import moment from 'moment';

/**
 * id INTEGER PRIMARY KEY,
    habitId TEXT NOT NULL,
    value INTEGER,
    date TEXT NOT NULL,
 */
type HabitMeasurementProps = {
    id: number | null,
    habitId: number,
    value: number | null,
    date: string,
    ref: any,
    type: string,
}

const ChangeHabitMeasurementComponent: React.FC<HabitMeasurementProps> = forwardRef((props: HabitMeasurementProps, ref) => {
  const [habitId, setHabitId] = useState(props.habitId);
  const [value, setValue] = useState(props.value ?? null);
  const [date, setDate] = useState(props.date ?? moment(new Date()).format('YYYY-MM-DDTHH:mm'));

  // dispatch necesary to reload the data in store
  // TODO switch to new action for adding data from form rather then reading from db if adding external database, for sqlite this is fine
  const dispatch = useDispatch<AppDispatch>();

  useImperativeHandle(ref, () => ({
    externalSubmit: async () => {
      console.log('SUBMITTING');
      await handleSubmit();
      dispatch(getAllHabitMeasurements());
      return;
    },
 }));

 const dataValidation = (errors: any[]) => {
    if (!habitId) {
        errors.push('Habit id not defined');
    }
    if (date == null)
    {
        errors.push('date not defined');
    }

    const dateSplit = date.split('T');
    if (dateSplit.length !== 2)
    {
        errors.push('INCORRECT DATE FORMAT');
    }
    else
    {
        const time = dateSplit[1]
        const timeSplit = time.split(':')
        if (!/[0-2][0-9]:[0-6][0-9]/.test(time) || parseInt(timeSplit[0], 10) > 23 || parseInt(timeSplit[0], 10) > 59)
        {
            errors.push('INCORRECT TIME FORMAT');
        }

        const dayDate = dateSplit[0];
        if (!/[0-3][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]/.test(dayDate))
        {
            errors.push('INCORRECT DATE FORMAT');
        }
    }
 }

  const handleSubmit = async () => {
    const errors: any[] = [];
    dataValidation(errors);

    if (errors.length >= 1)
    {
        Alert.alert(errors.join('/n'));
        return;
    }

    try {
        const dbConn = await DatabaseInstance.getConnection();
        if (props.type === 'insert')
        {
          await insertHabitMeasurement(
            dbConn,
            habitId,
            value,
            date);
        }
        else
        {
          if (props.id == null)
          {
            console.warn('MISSING HABBIT ID', props.id);
            return;
          }
          await updateHabitMeasurement( dbConn,
            props.id,
            habitId,
            value,
            date);
        }
        const allHabits = await selectAllHabits(dbConn);
        console.log(allHabits);
        Alert.alert('Success', 'Data saved successfully');
    } catch (error) {
      console.warn(error);
      Alert.alert('Error', 'Failed to save data');
    }
  };

  const handleCancel = async () => {
    console.log('cancel')
  }

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text
        style={styles.input}
      >{date}</Text>
      <DatePicker onChange={setDate} date={date}/>
    </ScrollView>
  );
});

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    padding: 16,
  },
  actionButtons: {
    flexDirection: 'row',
  },
  input: {
    height: 40,
    borderColor: '#ccc',
    borderWidth: 1,
    marginBottom: 12,
    padding: 8,
  },
});

export default ChangeHabitMeasurementComponent;