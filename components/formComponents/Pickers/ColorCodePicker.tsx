import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Modal, FlatList, StyleSheet, Button, TouchableWithoutFeedback } from 'react-native';

type ColorCodePickerProps = {
  value: string | null;
  onChange: (colorCode: string) => void;
};

const colors = [
  '#FF0000', '#00FF00', '#0000FF', '#FFFF00', '#FF00FF', '#00FFFF', '#000000', '#FFFFFF'
];

const ColorCodePicker: React.FC<ColorCodePickerProps> = ({ value, onChange }) => {
  const [modalVisible, setModalVisible] = useState(false);

  const handleColorSelect = (color: string) => {
    onChange(color);
    setModalVisible(false);
  };

  return (
    <View>
      <TouchableOpacity
        style={[styles.colorDisplay, { backgroundColor: value ?? '#000000' }]}
        onPress={() => setModalVisible(true)}
      >
        <Text style={styles.colorText}>{value ?? 'Select a color'}</Text>
      </TouchableOpacity>

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => setModalVisible(false)}
      >
        <TouchableWithoutFeedback onPress={() => setModalVisible(false)}>
          <View style={styles.modalOverlay}>
          <TouchableWithoutFeedback>
            <View style={styles.modalContainer}>
              <FlatList
                data={colors}
                keyExtractor={(item) => item}
                numColumns={4}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    style={[styles.colorOption, { backgroundColor: item }]}
                    onPress={() => handleColorSelect(item)}
                  />
                )}
              />
              <Button title="Cancel" onPress={() => setModalVisible(false)} />
            </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  colorDisplay: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 12,
  },
  colorText: {
    color: '#FFFFFF',
  },
  modalOverlay: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalContainer: {
    backgroundColor: 'white',
    padding: 16,
    borderRadius: 8,
    width: '80%',
    height: '50%',
    justifyContent: 'space-between',
  },
  colorOption: {
    width: 50,
    height: 50,
    margin: 8,
  },
});

export default ColorCodePicker;