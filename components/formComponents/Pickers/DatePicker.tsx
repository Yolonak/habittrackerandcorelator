import React, { useState } from "react";
import { Button, View, StyleSheet } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';

type DatePicketProps = {
    onChange: (time: string) => void;
    date?: string;
};

const DatePicker: React.FC<DatePicketProps> = ({ onChange, date }) => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date: Date) => {
    const dateStr = moment(date).format('YYYY-MM-DDTHH:mm');
    console.log(dateStr);
    onChange(dateStr);
    hideDatePicker();
  };

  return (
    <View style={styles.flex}>
      <Button title="Show Date Picker" onPress={showDatePicker} />
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="datetime"
        date={(date != null) ? moment(date, 'YYYY-MM-DDTHH:mm').toDate() : new Date()}
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
    </View>
  );
};

const styles = StyleSheet.create({
    flex: {
        justifyContent: "space-evenly",
    },
  });

export default DatePicker;