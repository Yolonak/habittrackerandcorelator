import React, { useCallback, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, FlatList, Pressable } from 'react-native';
import { scale } from "react-native-size-matters";

const daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

type DayPickerProps = {
    reminderDays: string;
    onChange: (days: string) => void;
};

const DayPicker: React.FC<DayPickerProps> = ({ reminderDays, onChange }) => {


const [selectedDays, setSelectedDays] = useState<Set<string>>(
    (reminderDays === '') ? new Set() : new Set(reminderDays?.replaceAll(' ', '').split(',')) ?? []
    );
  const toggleDay = (day: string) => {
    let newSelectedDays: Set<string>;
    if (selectedDays.has(day)) {
      selectedDays.delete(day);
      newSelectedDays = selectedDays;
    } else {
      newSelectedDays = new Set([...selectedDays, day]);
    }
    const strFromSet = Array.from(newSelectedDays).join(',');
    onChange(strFromSet);
    setSelectedDays(newSelectedDays);
  };

  const isActiveDay = useCallback(
    (thisDay: string, days: Set<string>) => {
      return days.has(thisDay);
    },
    []
  );

  return (
    <View style={styles.flex}>
        <View style={styles.daysContainer}>
          {daysOfWeek.map((day, index) => {
            return (
              <Pressable key={index} onPress={() => toggleDay(day)}>
                <View
                  style={[
                    styles.day,
                    isActiveDay(day, selectedDays) && styles.daySelected,
                  ]}
                >
                  <Text>
                    {day}
                  </Text>
                </View>
              </Pressable>
            );
          })}
        </View>
      </View>
  );
};

const styles = StyleSheet.create({
  dayOption: {
    padding: 10,
    margin: 5,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    backgroundColor: '#f9f9f9'
  },
  selectedDayOption: {
    backgroundColor: '#007bff',
    borderColor: '#007bff'
  },
  dayText: {
    color: '#000'
  },
  selectedDayText: {
    color: '#fff'
  },
  day: {
    width: scale(32),
    height: scale(32),
    justifyContent: "center",
    alignItems: "center",
    opacity: 0.6,
  },
  daysContainer:{
    flexDirection: "row",
    justifyContent: "space-evenly",
    backgroundColor: '#00000077',
    paddingVertical: scale(16),
    borderRadius: scale(12),
  },
  daySelected:{
    borderWidth: scale(1),
    borderRadius: scale(32),
    borderColor: '#f6be24',
    opacity: 1,
  },
  flex: { flex: 1 },
});

export default DayPicker;