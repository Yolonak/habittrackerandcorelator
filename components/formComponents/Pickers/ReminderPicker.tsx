import React, { useState } from 'react';
import { View, StyleSheet, Button } from 'react-native';
import DayPicker from './DayPicker';

type ReminderPickerProps = {
  remindersDay: string | null;
  remindersTime: string | null;
  onChange: (days: string, time: string) => void;
};

const ReminderPicker: React.FC<ReminderPickerProps> = ({ remindersDay, remindersTime, onChange }) => {
  const [selectedDays, setSelectedDays] = useState<string>(remindersDay ? remindersDay : '');
  const [selectedTime, setSelectedTime] = useState<string>(remindersTime || '');

  const handleSave = () => {
    const days = selectedDays;
    onChange(days, selectedTime);
  };

  return (
    <View style={styles.container}>
      <DayPicker reminderDays={selectedDays} onChange={setSelectedDays} />
      <Button title="Save" onPress={handleSave} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
});

export default ReminderPicker;