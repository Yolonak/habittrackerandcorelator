import React, { useState } from "react";
import { Button, View, StyleSheet } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

type TimePickerProps = {
    onChange: (time: string) => void;
};

const TimePicker: React.FC<TimePickerProps> = ({ onChange }) => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (time: Date) => {
    const timeStr = `${time.getHours()}:${time.getMinutes()}`
    onChange(timeStr);
    hideDatePicker();
  };

  return (
    <View style={styles.flex}>
      <Button title="Show Date Picker" onPress={showDatePicker} />
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="time"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
    </View>
  );
};

const styles = StyleSheet.create({
    flex: {
        justifyContent: "space-evenly",
    },
  });

export default TimePicker;