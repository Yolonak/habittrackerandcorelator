import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Modal, FlatList, StyleSheet, Button, TouchableWithoutFeedback, Image } from 'react-native';
import { useSelector } from 'react-redux';
import { RootState } from '../../../store/store';
import { getSpecificIcon } from '../../../assetLoading/iconLoader';

type IconPickeerProps = {
  value: number | null;
  onChange: (iconId: number) => void;
};

const IconPickeer: React.FC<IconPickeerProps> = ({ value, onChange }) => {
  const [modalVisible, setModalVisible] = useState(false);

  const { iconList } = useSelector((state: RootState) => state.icons);

  console.log(iconList);

  const handleIconSelect = (iconId: number) => {
    onChange(iconId);
    setModalVisible(false);
  };

  const selectedIcon = iconList.find(icon => icon.id === value);

  // { uri: `asset:/${selectedIcon.nameOfAsset}` } try with this
  return (
    <View>
      <TouchableOpacity
        style={styles.iconDisplay}
        onPress={() => setModalVisible(true)}
      >
        {selectedIcon ? (
          <Image source={getSpecificIcon(selectedIcon.nameOfAsset)} style={styles.icon} />
        ) : (
          <Text style={styles.iconText}>Select an icon</Text>
        )}
      </TouchableOpacity>

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => setModalVisible(false)}
      >
        <TouchableWithoutFeedback onPress={() => setModalVisible(false)}>
          <View style={styles.modalOverlay}>
          <TouchableWithoutFeedback>
          <View style={styles.modalContainer}>
            <FlatList
              data={iconList}
              keyExtractor={(item) => item.id.toString()}
              numColumns={4}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={styles.iconOption}
                  onPress={() => handleIconSelect(item.id)}
                >
                  <Image source={getSpecificIcon(item.nameOfAsset)} style={styles.icon} />
                </TouchableOpacity>
              )}
            />
            <Button title="Cancel" onPress={() => setModalVisible(false)} />
          </View>
          </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  iconDisplay: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 12,
  },
  iconText: {
    color: '#000000',
  },
  modalOverlay: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalContainer: {
    backgroundColor: 'white',
    padding: 16,
    borderRadius: 8,
    width: '80%',
    height: '50%',
    justifyContent: 'space-between',
  },
  iconOption: {
    width: 50,
    height: 50,
    margin: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: 40,
    height: 40,
  },
});

export default IconPickeer;