import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { View, Text, TouchableOpacity, FlatList, Modal, StyleSheet, Button } from 'react-native';
import { RootState } from '../../../store/store';

type GroupIdPickerProps = {
    type: string;
    value: number | null;
    onChange: (groupId: number, groupName: string) => void;
};

const GroupIdPicker: React.FC<GroupIdPickerProps> = ({ type, value, onChange }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const { habitGroupList } = useSelector((state: RootState) => state.habits);

  const handleGroupSelect = (groupId: number, groupName: string) => {
    onChange(groupId, groupName);
    setModalVisible(false);
  };

  // TODO make more generic, allo wfor both habits and metric
  const selectedGroup = (type === 'habit') ? habitGroupList.find(group => group.id === value) : {id: 1, name: 'defaultMetric'};

  return (
    <View>
      <TouchableOpacity
        style={styles.groupDisplay}
        onPress={() => setModalVisible(true)}
      >
        <Text style={styles.groupText}>
          {selectedGroup ? selectedGroup.name : 'Select a group'}
        </Text>
      </TouchableOpacity>

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => setModalVisible(false)}
      >
        <TouchableOpacity
          style={styles.modalOverlay}
          activeOpacity={1}
          onPressOut={() => setModalVisible(false)}
        >
          <View style={styles.modalContainer}>
            <FlatList
              data={(type === 'habit') ? habitGroupList:[]}
              keyExtractor={(item) => item.id.toString()}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={styles.groupOption}
                  onPress={() => handleGroupSelect(item.id, item.name)}
                >
                  <Text style={styles.groupOptionText}>{item.name}</Text>
                </TouchableOpacity>
              )}
            />
            <Button title="Cancel" onPress={() => setModalVisible(false)} />
          </View>
        </TouchableOpacity>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  groupDisplay: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 12,
  },
  groupText: {
    color: '#000',
  },
  modalOverlay: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalContainer: {
    backgroundColor: 'white',
    padding: 16,
    borderRadius: 8,
    width: '80%',
    height: '50%',
    justifyContent: 'space-between',
  },
  groupOption: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  groupOptionText: {
    fontSize: 16,
  },
});

export default GroupIdPicker;