import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Modal, FlatList, StyleSheet, Button, TouchableWithoutFeedback } from 'react-native';
import DayPicker from './DayPicker';
import TimePicker from './TimePicker';

type ReminderPickerProps = {
    remindersDay: string | null;
    remindersTime: string | null;
    onChange: (days: string, time: string) => void;
  };

const ReminderModalPicker: React.FC<ReminderPickerProps> = ({ remindersDay, remindersTime, onChange }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedDays, setSelectedDays] = useState<string>(remindersDay ? remindersDay : '');
  const [selectedTime, setSelectedTime] = useState<string>(remindersTime || '');

  const remindersDayOriginal = remindersDay ?? '';
  const remindersTimeOriginal = remindersTime ?? '';

  const handleSave = () => {
    const days = selectedDays;
    onChange(days, selectedTime);
    setModalVisible(false);
  };

  const handleCancel = () => {
    setSelectedDays(remindersDayOriginal);
    setSelectedTime(remindersTimeOriginal);
    onChange(remindersDayOriginal, remindersTimeOriginal);
    setModalVisible(false);
  }

  return (
    <View>
      <TouchableOpacity
        style={[styles.colorDisplay, { backgroundColor: '#000000' }]}
        onPress={() => setModalVisible(true)}
      >
        <Text style={styles.colorText}>{`Days: ${selectedDays}`}</Text>
        <Text style={styles.colorText}>{`Time: ${selectedTime}`}</Text>
      </TouchableOpacity>

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => setModalVisible(false)}
      >
        <TouchableWithoutFeedback onPress={() => setModalVisible(false)}>
          <View style={styles.modalOverlay}>
          <TouchableWithoutFeedback>
            <View style={styles.modalContainer}>
                <View style={styles.container}>
                    <DayPicker reminderDays={selectedDays} onChange={setSelectedDays} />
                    <TimePicker onChange={setSelectedTime} />
                </View>
                <Button title="Save" onPress={handleSave} />
                <Button title="Cancel" onPress={() => handleCancel()} />
            </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  colorDisplay: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 12,
  },
  colorText: {
    color: '#FFFFFF',
  },
  modalOverlay: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalContainer: {
    backgroundColor: 'white',
    padding: 16,
    borderRadius: 8,
    width: '80%',
    height: '50%',
    justifyContent: 'space-between',
  },
  container: {
    padding: 5,
    justifyContent: 'space-between',
    width: '100%',
    height: '50%',
  },
});

export default ReminderModalPicker;