import React, { useState, forwardRef, useImperativeHandle } from "react";
import {
  View,
  TextInput,
  Button,
  StyleSheet,
  Alert,
  ScrollView,
  Switch,
  Text,
} from "react-native";

// Assuming this method exists in another file and is imported here
import DatabaseInstance from "../../SQLite/sqlInit";
import {
  habitDataValidation,
  insertHabit,
  selectAllHabits,
  updateHabit,
} from "../../SQLite/habitOperations/habitOperations";
import { useDispatch } from "react-redux";
import { AppDispatch } from "../../store/store";
import { getAllHabits } from "../../store/habitActions";
import ColorCodePicker from "./Pickers/ColorCodePicker";
import IconPicker from "./Pickers/IconPicker";
import ReminderModalPicker from "./Pickers/ReminderModalPicker";
import GroupIdPicker from "./Pickers/GroupIdPicker";

type HabitProps = {
  id: number | null;
  name: string | null;
  iconId: number | null;
  groupId: number | null;
  // groupName?: string | null;
  colorCode: string | null;
  remindersDay: string | null;
  remindersTime: string | null;
  targetQuantity: number | null;
  habitOrder: number | null;
  qualityValue: number | null;
  minQualityValue?: number | null;
  maxQualityValue?: number | null;
  timeRelevant?: number | null;
  description: string | null;
  ref: any;
  type: string;
};

const ChangeHabitComponent: React.FC<HabitProps> = forwardRef(
  (props: HabitProps, ref) => {
    const [name, setName] = useState(props.name ?? "");
    const [iconId, setIconId] = useState(props.iconId);
    const [groupId, setGroupId] = useState(props.groupId);
    // const [groupName, setGroupName] = useState<string | null>(props.groupName ?? null);
    const [colorCode, setColorCode] = useState(props.colorCode ?? "");
    const [remindersDay, setRemindersDay] = useState(props.remindersDay ?? "");
    const [remindersTime, setRemindersTime] = useState(
      props.remindersTime ?? ""
    );
    const [targetQuantity, setTargetQuantity] = useState(
      props.targetQuantity ?? 1
    );
    const [habitOrder, setHabitOrder] = useState(
      props.habitOrder?.toString() ?? "5"
    );
    const [qualityValue, setQualityValue] = useState(
      (props.qualityValue === 1) ? true : false
    );
    const [minQualityValue, setMinQualityValue] = useState(
      props.minQualityValue ?? null
    );
    const [maxQualityValue, setMaxQualityValue] = useState(
      props.maxQualityValue ?? null
    );
    const [timeRelevant, setTimeRelevant] = useState(
      (props.timeRelevant === 1) ? true : false
    );
    const [description, setDescription] = useState(props.description ?? "");

    const [hasReminder, setHasReminder] = useState(
      ((props.remindersDay !== '' && props.remindersDay != null) || (props.remindersTime !== '' && props.remindersTime != null)) ? true : false
    );
    const [reminderInput, setReminderInput] = useState<string>(`${props.remindersDay?.replaceAll(' ', '')} ${props.remindersTime?.replaceAll(' ', '')}`);

    // dispatch necesary to reload the data in store
    // TODO switch to new action for adding data from form rather then reading from db if adding external database, for sqlite this is fine
    const dispatch = useDispatch<AppDispatch>();

    useImperativeHandle(ref, () => ({
      externalSubmit: async () => {
        console.log("SUBMITTING");
        await handleSubmit();
        dispatch(getAllHabits());
        return;
      },
    }));

    const dataValidation = (errors: any[]) => {
      if (!name) {
        errors.push("Error", "Name required");
      }
      if (iconId == null) {
        errors.push("Error", "Icon required");
      }
      if (groupId == null) {
        errors.push("Error", "Group required");
      }
      if (qualityValue) {
        if (
          minQualityValue == null ||
          maxQualityValue == null ||
          maxQualityValue <= minQualityValue
        ) {
          errors.push(
            "WRONG VALUES, BOTH MUST BE SET AND MAX MUST ME LARGER THEN MIN"
          );
        }
      }
      if (remindersDay.length > 1) {
        const splitDays = remindersDay.split(",");
        const daySet = new Set([
          "Mon",
          "Tue",
          "Wed",
          "Thu",
          "Fri",
          "Sat",
          "Sun",
        ]);
        for (const el of splitDays) {
          if (!daySet.has(el.replaceAll(' ', ''))) {
            errors.push(`WRONG DAY SYMBOL: ${el}`);
          }
        }
      }

      if (remindersTime.length > 1) {
        const splitTime = remindersTime.split(",");
        for (const el of splitTime) {
          if (!/[0-2][0-9]:[0-6][0-9]/.test(el)) {
            errors.push("WRONG TIME");
          }
        }
      }
    };

    const handleSubmit = async () => {
      const errors: any[] = [];
      dataValidation(errors);
      if (errors.length > 0) {
        Alert.alert(errors.join("/n"));
        return;
      }
      // TODO see how to not require this check in typescript
      if (iconId == null || groupId == null) {
        return;
      }

      try {
        const dbConn = await DatabaseInstance.getConnection();
        const remindersDayComputed = (hasReminder) ? remindersDay : '';
        const remindersTimeComputed = (hasReminder) ? remindersTime : '';
        if (props.type === "insert") {
          await insertHabit(
            dbConn,
            name,
            iconId,
            groupId,
            colorCode,
            remindersDayComputed,
            remindersTimeComputed,
            targetQuantity,
            parseInt(habitOrder, 10),
            qualityValue,
            minQualityValue,
            maxQualityValue,
            timeRelevant,
            description
          );
        } else {
          if (props.id == null) {
            console.warn("MISSING HABBIT ID", props.id);
            return;
          }
          await updateHabit(
            dbConn,
            props.id,
            name,
            iconId,
            groupId,
            colorCode,
            remindersDayComputed,
            remindersTimeComputed,
            targetQuantity,
            parseInt(habitOrder, 10),
            qualityValue,
            minQualityValue,
            maxQualityValue,
            timeRelevant,
            description
          );
        }
        const allHabits = await selectAllHabits(dbConn);
        console.log(allHabits);
        Alert.alert("Success", "Data saved successfully");
        handleCancel();
      } catch (error) {
        console.warn(error);
        Alert.alert("Error", "Failed to save data");
      }
    };

    const handleCancel = async () => {
      setName("");
      setIconId(null);
      setGroupId(null);
      setColorCode("");
      setRemindersDay("");
      setRemindersTime("");
      setTargetQuantity(1);
      setHabitOrder("5");
      setQualityValue(false);
      setMinQualityValue(null);
      setMaxQualityValue(null);
      setTimeRelevant(false);
      setDescription("");
    };

    const handleReminderChange = (days: string, time: string) => {
      setRemindersDay(days);
      setRemindersTime(time);
    };

    const handleGroupIdChange = (id: number, name: string) => {
      setGroupId(id);
      // setGroupName(name);
    }

    return (
      <ScrollView contentContainerStyle={styles.container}>
      <Text>{'Name'}</Text>
      <TextInput
        style={styles.input}
        placeholder="Name"
        value={name ?? ''}
        onChangeText={setName}
      />
      {/* <TextInput
        style={styles.input}
        placeholder="Icon ID"
        value={iconId ? String(iconId) : ''}
        onChangeText={(text) => setIconId(Number(text))}
        keyboardType="numeric"
    /> */ }
      <Text>{'Icon'}</Text>
      <IconPicker value={iconId} onChange={setIconId} />
      <Text>{'Group ID'}</Text>
      <GroupIdPicker value={groupId} onChange={handleGroupIdChange} type={'habit'}/>
      {/* <TextInput
        style={styles.input}
        placeholder="Group ID"
        value={groupId ? String(groupId) : ''}
        onChangeText={(text) => setGroupId(Number(text))}
        keyboardType="numeric"
  /> */}
      { /* <TextInput
        style={styles.input}
        placeholder="Color Code"
        value={colorCode ?? ''}
        onChangeText={setColorCode}
    /> */}
      <Text>{'Color'}</Text>
      <ColorCodePicker value={colorCode} onChange={setColorCode} />
      {/* <TextInput
        style={styles.input}
        placeholder="Reminders (e.g., Mon,Tue 20:00,22:00)"
        value={reminderInput}
        onChangeText={handleReminderInputChange}
  /> */}
      <View style={styles.switchContainer}>
        <Text>Reminder</Text>
        <Switch
          value={hasReminder}
          onValueChange={(value) => setHasReminder(value)}
        />
      </View>
      {hasReminder && (
        <>
          <ReminderModalPicker
            remindersDay={remindersDay}
            remindersTime={remindersTime}
            onChange={handleReminderChange}
          />
        </>
      )}
      <Text>Target Daily Amount</Text>
      <TextInput
        style={styles.input}
        placeholder="Target Quantity"
        value={targetQuantity ? String(targetQuantity) : ''}
        onChangeText={(text) => setTargetQuantity(Number(text))}
        keyboardType="numeric"
      />
      <Text>Habit Order</Text>
      <TextInput
        style={styles.input}
        placeholder="Habit Order"
        value={habitOrder ? String(habitOrder) : ''}
        onChangeText={(text) => setHabitOrder(text)}
        keyboardType="numeric"
      />
      <View style={styles.switchContainer}>
        <Text>Quality Value</Text>
        <Switch
          value={qualityValue}
          onValueChange={(value) => setQualityValue(value)}
        />
      </View>
      {qualityValue && (
        <>
        <Text>Min Quality Value</Text>
          <TextInput
            style={styles.input}
            placeholder="Min Quality Value"
            value={minQualityValue ? String(minQualityValue) : ''}
            onChangeText={(text) => setMinQualityValue(Number(text))}
            keyboardType="numeric"
          />
          <Text>Max Quality Value</Text>
          <TextInput
            style={styles.input}
            placeholder="Max Quality Value"
            value={maxQualityValue ? String(maxQualityValue) : ''}
            onChangeText={(text) => setMaxQualityValue(Number(text))}
            keyboardType="numeric"
          />
        </>
      )}
      <View style={styles.switchContainer}>
        <Text>Time Relevant</Text>
        <Switch
          value={timeRelevant ?? false}
          onValueChange={(value) => setTimeRelevant(value)}
        />
      </View>
      <Text>Description</Text>
      <TextInput
        style={styles.input}
        placeholder="Description"
        value={description ?? ''}
        onChangeText={setDescription}
      />
    </ScrollView>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
  input: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    marginBottom: 12,
    padding: 8,
  },
  switchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 12,
  },
});

export default ChangeHabitComponent;
