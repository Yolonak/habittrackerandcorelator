import React, { useState, useRef } from 'react';
import { View, TextInput, StyleSheet, Alert, ScrollView } from 'react-native';
import { Dialog, Portal, Button } from 'react-native-paper';

type HabitGroupProps = {
    id: number | null,
    name: string | null,
    description: string | null,
    grpOrder: number | null,
  }

const HabitGroupDialog: React.FC<HabitGroupProps> = (props) => {
  const [visible, setVisible] = useState(false);
  const [name, setName] = useState(props.name ?? '');
  const [description, setDescription] = useState(props.description ?? '');
  const [groupOrder, setGroupOrder] = useState(props.grpOrder?.toString() ?? '5');

  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);

  const handleDialogContentSubmit = async() => {
    // ?????
  }

  return (
        <Portal>
          <Dialog visible={visible} onDismiss={hideDialog}>
            <View style={{height: 300, width: 300, paddingBottom: 105}}>
              <Dialog.Title>Edit</Dialog.Title>
              <Dialog.Content>
                <ScrollView contentContainerStyle={styles.dialogContainer}>
                    <TextInput
                        style={styles.input}
                        placeholder="Name"
                        value={name}
                        onChangeText={setName}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder="Description"
                        value={description}
                        onChangeText={setDescription}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder="Group Order"
                        value={groupOrder}
                        onChangeText={setGroupOrder}
                        keyboardType="numeric"
                    />
                </ScrollView>
              </Dialog.Content>
              <Dialog.Actions>
                <Button onPress={handleDialogContentSubmit}>Submit</Button>
                <Button onPress={hideDialog}>Cancel</Button>
              </Dialog.Actions>
            </View>
          </Dialog>
        </Portal>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'relative',
  },
  dialogContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    padding: 16,
  },
  button: {
    position: 'absolute',
    right: 10,
    top: 10,
    backgroundColor: 'white',
    padding: 8,
    borderRadius: 16,
    elevation: 2, // Add shadow for Android
    shadowColor: '#000', // Add shadow for iOS
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
  },
  actionButtons: {
    flexDirection: 'row',
  },
  input: {
    height: 40,
    borderColor: '#ccc',
    borderWidth: 1,
    marginBottom: 12,
    padding: 8,
  },
});

export default HabitGroupDialog;