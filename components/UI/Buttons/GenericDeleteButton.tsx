import React, { useState } from 'react';
import { StyleSheet, View, TouchableOpacity, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Dialog, Portal, Button, Text } from 'react-native-paper';

type DeleteButtonProps = {
    onDelete: (id: number) => Promise<void>,
    params: any,
    confirmationMessage?: string
}

const GenericDeleteButton: React.FC<DeleteButtonProps> = ({ onDelete, params, confirmationMessage = "Are you sure you want to delete this item?" }) => {
  const [visible, setVisible] = useState(false);

  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);

  const handleDelete = async () => {
    await onDelete(params.id);
    hideDialog();
  }

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.button} onPress={showDialog}>
        <Icon name="delete" size={24} color="black" />
      </TouchableOpacity>

      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <View style={{ padding: 20 }}>
            <Dialog.Title>Delete</Dialog.Title>
            <Dialog.Content>
              <Text>{confirmationMessage}</Text>
            </Dialog.Content>
            <Dialog.Actions>
              <Button onPress={handleDelete}>Delete</Button>
              <Button onPress={hideDialog}>Cancel</Button>
            </Dialog.Actions>
          </View>
        </Dialog>
      </Portal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  button: {
    position: 'relative',
    right: 10,
    backgroundColor: 'white',
    padding: 8,
    borderRadius: 16,
    elevation: 2, // Add shadow for Android
    shadowColor: '#000', // Add shadow for iOS
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
  },
});

export default GenericDeleteButton;