import React, { useState, useRef } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Dialog, Portal, Button } from 'react-native-paper';

type EditButtonProps = {
    props: any,
    component: React.FC<any>
  }

type RefType = {
    externalSubmit: () => void
}

const GenericEditButton: React.FC<EditButtonProps> = ({props, component}) => {
  const [visible, setVisible] = useState(false);
  const submitFormRef = useRef<RefType>();
  const Component = component;

  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);

  const handleDialogContentSubmit = async() => {
    if (submitFormRef.current != null)
    {
        await submitFormRef.current?.externalSubmit();
        hideDialog();
    }
  }

  const childProps = {
    ...props,
    ref: submitFormRef,
  }

  return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.button} onPress={showDialog}>
          <Icon name="edit" size={24} color="black" />
        </TouchableOpacity>

        <Portal>
          <Dialog visible={visible} onDismiss={hideDialog}>
            <View style={{height: '90%', width: '100%', paddingBottom: 105}}>
              <Dialog.Title>Edit</Dialog.Title>
              <Dialog.Content>
                  {<Component
                        {...childProps} />}
              </Dialog.Content>
              <Dialog.Actions>
                <Button onPress={handleDialogContentSubmit}>Submit</Button>
                <Button onPress={hideDialog}>Cancel</Button>
              </Dialog.Actions>
            </View>
          </Dialog>
        </Portal>
      </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  dialogContainer: {
  },
  button: {
    position: 'relative',
    right: 10,
    backgroundColor: 'white',
    padding: 8,
    borderRadius: 16,
    elevation: 2, // Add shadow for Android
    shadowColor: '#000', // Add shadow for iOS
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
  },
});

export default GenericEditButton;