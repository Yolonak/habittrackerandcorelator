import React, { useState } from 'react';
import { Appbar, IconButton, Portal, Dialog, Button, Text } from 'react-native-paper';
import { StackHeaderProps } from '@react-navigation/stack';
import { useNavigation } from '@react-navigation/native';
import GenericInsertButton from '../UI/Buttons/GenericInsertButton';
import ChangeHabitGroupComponent from '../formComponents/ChangeHabitGroupComponent';

const HabitHeaderComponent: React.FC<StackHeaderProps> = ({ navigation, options }) => {
  if (globalThis.debugMode)
    {
      console.log('RENDER HabitHeaderComponent')
    }
  const [settingsVisible, setSettingsVisible] = useState(false);
  const [addVisible, setAddVisible] = useState(false);

  const openSettingsDialog = () => setSettingsVisible(true);
  const closeSettingsDialog = () => setSettingsVisible(false);

  const openAddDialog = () => setAddVisible(true);
  const closeAddDialog = () => setAddVisible(false);

  const childPropsInsertButton = {
    props: {
      id: null,
      habitId: null,
      date: null,
      value: null,
      type: 'insert',
    },
    component: ChangeHabitGroupComponent
  }

  return (
    <Appbar.Header>
      {navigation.canGoBack() ? (
        <Appbar.BackAction onPress={navigation.goBack} />
      ) : (
        <IconButton icon="cog" onPress={openSettingsDialog} />
      )}
      <Appbar.Content title={options.title} />
      {/*<IconButton icon="plus" onPress={openAddDialog} />*/}
      <GenericInsertButton {...childPropsInsertButton} />

      <Portal>
        <Dialog visible={settingsVisible} onDismiss={closeSettingsDialog}>
          <Dialog.Title>Settings</Dialog.Title>
          <Dialog.Content>
            <Text>Settings content goes here.</Text>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={closeSettingsDialog}>Done</Button>
          </Dialog.Actions>
        </Dialog>
        <Dialog visible={addVisible} onDismiss={closeAddDialog}>
          <Dialog.Title>Add</Dialog.Title>
          <Dialog.Content>
            <Text>Add content goes here.</Text>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={closeAddDialog}>Done</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </Appbar.Header>
  );
};

export default HabitHeaderComponent;