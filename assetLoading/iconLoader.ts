/* eslint-disable import/prefer-default-export */
const favicon = require('../assets/favicon.png')
const icon = require('../assets/icon.png');
const splash = require('../assets/splash.png');

export const getSpecificIcon = (path: string) => {
  switch (path) {
    case 'favicon':
        return favicon;
    case 'icon':
        return icon;
    case 'splash':
        return splash;
      // Add more cases for other predefined icons as needed

    default:
      // If the provided path doesn't match any predefined icons, use a default icon
      return icon;
  }
};
