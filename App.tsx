/* eslint-disable react/style-prop-object */
import React, { useEffect, useState } from 'react';
import {
  View, Text,
} from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {
  MenuProvider,
} from 'react-native-popup-menu';
// import { Ionicons } from '@expo/vector-icons';
import MainDisplayScreen from './screens/MainDisplayScreen';
import DatabaseInstance from './SQLite/sqlInit';
import { Provider as PaperProvider } from 'react-native-paper';
import { store } from './store/store'
import { Provider } from 'react-redux'

const Stack = createNativeStackNavigator();
export default function App() {
  const [initialized, setInitialized] = useState<boolean>(false);
  globalThis.debugMode = true; // set to false in prod
  useEffect(() => {
    async function initialize() {
      await DatabaseInstance.init();
      setInitialized(true);
    }
    initialize();
  }, [])
  // TODO better loading screen
  if (!initialized)
  {
    return (
      <>
        <Text>{'LOADING'}</Text>
      </>
    )
  }
  return (
    <>
      <StatusBar style="light" />
      <>
        {/*
          Context provider here
        */}
          <Provider store={store}>
            <PaperProvider>
              <MenuProvider>
                <NavigationContainer>
                  <Stack.Navigator
                    screenOptions={{
                      headerShown: false
                    }}
                  >
                    <Stack.Screen
                      name="MainDisplayScreen"
                      component={MainDisplayScreen}
                    />
                  </Stack.Navigator>
                </NavigationContainer>
              </MenuProvider>
            </PaperProvider>
          </Provider>
      </>
    </>
  );
}
