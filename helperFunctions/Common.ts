export const findDuplicates = (arr: any[]) => arr.filter((item, index) => arr.indexOf(item) !== index)

export const findMaxByField = (arr: any[], fieldName: string) => {
    let maxIndex = 0;
    let maxValue;
    for (let i = 0; i < arr.length; i++)
    {
        if (maxValue == null)
        {
            maxValue = arr[i][fieldName];
            continue;
        }
        if (arr[i][fieldName] > maxValue)
        {
            maxIndex = i;
            maxValue = arr[i][fieldName];
        }
    }
    return maxValue;
}