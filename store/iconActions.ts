import { createAsyncThunk } from "@reduxjs/toolkit";
import DatabaseInstance from "../SQLite/sqlInit";
import { IconData } from "../commonTypes/PropTypes";
import { getAllIconsAsync } from "../SQLite/otherOperations/iconOperations";

export const getAllIcons = createAsyncThunk(
    'getAllIcons',
    async (): Promise<IconData[]> => {
        try
        {
            const dbConn = await DatabaseInstance.getConnection();
            const iconData = await getAllIconsAsync(dbConn);
            return iconData;
        }
        catch (error)
        {
            console.warn(error);
            return [];
        }
    }
);