import { configureStore } from '@reduxjs/toolkit'
import counterSlice from './counterSlice'
import habitSlice from './habitReducer'
import iconSlice from './iconSlice'

export const store = configureStore({
  reducer: {
    counter: counterSlice,
    habits: habitSlice,
    icons: iconSlice,
  },
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch