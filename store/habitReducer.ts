import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { getAllHabitGroups, getAllHabitMeasurements, getAllHabits } from "./habitActions";

import { GroupData, HabitData, HabitMeasurementData } from '../commonTypes/PropTypes';

export interface HabitState {
    habitGroupList: GroupData[],
    habitList: HabitData[],
    habitMeasurementList: HabitMeasurementData[]
}

const initialState: HabitState = {
    habitGroupList: [],
    habitList: [],
    habitMeasurementList: []
}

export const habitSlice = createSlice({
    name: 'habits',
    initialState,
    reducers: {},
    extraReducers: builder => {
        builder.addCase(getAllHabitGroups.fulfilled, (state, action: PayloadAction<GroupData[]>) => {
            state = {...state, habitGroupList: action.payload}
            return state;
        });
        builder.addCase(getAllHabits.fulfilled, (state, action: PayloadAction<HabitData[]>) => {
            state = {...state, habitList: action.payload}
            return state;
        });
        builder.addCase(getAllHabitMeasurements.fulfilled, (state, action: PayloadAction<HabitMeasurementData[]>) => {
            state = {...state, habitMeasurementList: action.payload}
            return state;
        })
    }
})

export default habitSlice.reducer;