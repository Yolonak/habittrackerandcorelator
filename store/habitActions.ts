import { createAsyncThunk } from "@reduxjs/toolkit";
import { selectAllHabitGroups } from "../SQLite/habitOperations/habitGroupOperations";
import DatabaseInstance from "../SQLite/sqlInit";
import { GroupData, HabitData, HabitMeasurementData } from "../commonTypes/PropTypes";
import { selectAllHabitMeasurements } from "../SQLite/habitOperations/habitMeasurementOperations";
import { selectAllHabits } from "../SQLite/habitOperations/habitOperations";

export const getAllHabitGroups = createAsyncThunk(
    'getAllHabitGroups',
    async (): Promise<GroupData[]> => {
        try
        {
            const dbConn = await DatabaseInstance.getConnection();
            const habitGrpData = await selectAllHabitGroups(dbConn);
            return habitGrpData;
        }
        catch (error)
        {
            console.warn(error);
            return [];
        }
    }
);

export const getAllHabits = createAsyncThunk(
    'selectAllHabits',
    async (): Promise<HabitData[]> => {
        try
        {
            const dbConn = await DatabaseInstance.getConnection();
            const habitData = await selectAllHabits(dbConn);
            return habitData;
        }
        catch (error)
        {
            console.warn(error);
            return [];
        }
    }
);

export const getAllHabitMeasurements = createAsyncThunk(
    'selectAllHabitMeasurements',
    async (): Promise<HabitMeasurementData[]> => {
        try
        {
            const dbConn = await DatabaseInstance.getConnection();
            const habitMeasurementData = await selectAllHabitMeasurements(dbConn);
            return habitMeasurementData;
        }
        catch (error)
        {
            console.warn(error);
            return [];
        }
    }
);