import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { getAllIcons } from "./iconActions";

import { IconData } from '../commonTypes/PropTypes';

export interface IconState {
    iconList: IconData[],
}

const initialState: IconState = {
    iconList: [],
}

export const habitSlice = createSlice({
    name: 'habits',
    initialState,
    reducers: {},
    extraReducers: builder => {
        builder.addCase(getAllIcons.fulfilled, (state, action: PayloadAction<IconData[]>) => {
            state = {...state, iconList: action.payload}
            return state;
        });
    }
})

export default habitSlice.reducer;