export type GroupData = {
    id: number,
    name: string,
    description: string,
    grpOrder: number,
}

export type HabitMeasurementData = {
    id: number,
    habitId: string,
    value: number,
    date: string,
}

export type RootElementProps = {
    groupData: GroupData,
    expanded: boolean,
}

export type HabitRootElemetProps = {
    habitData: HabitData,
    expanded: boolean
}

export type HabitData = {
    id: number,
    name: string,
    iconId: number,
    groupId: number,
    colorCode: string,
    remindersDay: string,
    remindersTime: string,
    targetQuantity: number,
    habitOrder: number,
    qualityValue: boolean,
    minQualityValue?: number,
    maxQualityValue?: number,
    timeRelevant?: boolean,
    description: string
}

export type IconData = {
    id: number,
    nameOfAsset: string,
    keywords: string,
}